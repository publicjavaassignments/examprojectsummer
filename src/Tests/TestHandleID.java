package Tests;

import DryCleaning.Database.HandleID;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.abs;
import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertEquals;

public class TestHandleID
    {
    int upperBoundUniqueID = 999999;
    int upperBoundHumanReadable = 6471002;

    @Test
    public void testLengths() {
        long ID = HandleID.getUniqueId();
        assert String.valueOf(ID).length() == 19;
    }

    @Test
    public void testLengthsHumanReadable() {
        String ID = HandleID.getUniqueHumanReadableID();
        assert String.valueOf(ID).length() == 5;
    }

    @Test
    public void testDateConversion() {
        int maximumTimeDifference = 100;

        long ID = HandleID.getUniqueId();
        Date date = HandleID.getDate(ID);
        assert abs(date.getTime() - currentTimeMillis()) < maximumTimeDifference;
    }

    /*
    Generate unique IDs, add them to an array and trickle it through a HashSet.

    The HashSet will only take in unique values, meaning if two values are the same, the amountToGenerate will
    be less than that of the uniqueID HashSet size.
    This makes it significantly faster, since we don't have to compare every element with every other element.
     */
    @Test
    public void testUniqueGeneration() {
        Long[] GeneratedIDArray = new Long[upperBoundUniqueID];

        for (int i = 0; i < upperBoundUniqueID; i++) {
            GeneratedIDArray[i] = HandleID.getUniqueId();
        }

        Set <Long> uniqueID = new HashSet<> (upperBoundUniqueID);
        for (int i = 0; i < upperBoundUniqueID; i++) {
            uniqueID.add(GeneratedIDArray[i]);
        }

        assertEquals(uniqueID.size(), upperBoundUniqueID);
    }

        /*
        This test can take upwards of 20-60 seconds to complete, but it demonstrates that the program cannot output
        the same human readable ID within the same session. The test length is effectively the total time it takes
        to saturate the uniqueness pool within the chosen C(n,r) variables for the combinations.
         */
        @Test
        public void testUniqueGenerationHumanReadable() {
            String[] GeneratedIDArray = new String[upperBoundHumanReadable];

            for (int i = 0; i < upperBoundHumanReadable; i++) {
                GeneratedIDArray[i] = HandleID.getUniqueHumanReadableID();
            }

            Set <String> uniqueID = new HashSet<> (upperBoundHumanReadable);
            for (int i = 0; i < upperBoundHumanReadable; i++) {
                uniqueID.add(GeneratedIDArray[i]);
            }

            assertEquals(uniqueID.size(), upperBoundHumanReadable);
        }

    /*
    Tests to see if the generated array is actually monotonic in nature. If two incremented indices of the array hold
    long values which are not increasing, then the test will fail.
     */
    @Test
    public void testMonotonicity() {
        Long[] GeneratedIDArray = new Long[upperBoundUniqueID];

        for (int i = 0; i < upperBoundUniqueID; i++) {
            GeneratedIDArray[i] = HandleID.getUniqueId();
        }

        long previousID = -1;
        long length = GeneratedIDArray.length;

        for (int i = 0; i < length; i++) {
            long ID = GeneratedIDArray[i];
            if (previousID != -1) {
                if (previousID >= ID) {
                    assert false;
                } else {
                    assert true;
                }
            }
            previousID = ID;
        }
    }
}