package DryCleaning;

import DryCleaning.Application.Main.MainController;

import java.io.IOException;

import javafx.fxml.FXMLLoader;

public class SceneNavigator {
    public static final String MAIN = "DryCleaning/Presentation/View/Main/Main.fxml";
    public static final String MainMenu = "DryCleaning/Presentation/View/Main/MainMenu.fxml";
    public static final String LoginPrompt = "DryCleaning/Presentation/View/Main/LoginPrompt.fxml";
    public static final String SHOP_MENU = "DryCleaning/Presentation/View/Shop/ShopMenuAddOrder.fxml";
    public static final String SHOP_PRELIMINARY = "DryCleaning/Presentation/View/Shop/ShopMenuPreliminary.fxml";
    public static final String DRIVER_MENU = "DryCleaning/Presentation/View/Driver/DriverMenu.fxml";
    public static final String DRIVER_PRELIMINARY = "DryCleaning/Presentation/View/Driver/DriverMenuPreliminary.fxml";
    public static final String SHOP_PICKUP = "DryCleaning/Presentation/View/Shop/ShopMenuOrderPickup.fxml";
    public static final String CENTRAL_MENU = "DryCleaning/Presentation/View/Central/CentralMenuCentral.fxml";
    public static final String CENTRAL_MENU_UPCOMING = "DryCleaning/Presentation/View/Central/CentralMenuUpcoming.fxml";
    public static final String CENTRAL_PRELIMINARY = "DryCleaning/Presentation/View/Central/CentralMenuPreliminary.fxml";
    public static final String ADMIN_PANEL_STATISTICS = "DryCleaning/Presentation/View/Admin/AdminPanelStatistics.fxml";
    public static final String ADMIN_CREATE_USER = "DryCleaning/Presentation/View/Admin/AdminPanelCreateUser.fxml";
    public static final String ADMIN_EDIT_USER = "DryCleaning/Presentation/View/Admin/AdminPanelEditUser.fxml";
    public static final String ADMIN_NEW_DEPARTMENT = "DryCleaning/Presentation/View/Admin/AdminPanelCreateDepartment.fxml";


    private static MainController mainController;

    /**
     *
     * @param mainController
     */
    public static void setMainController(MainController mainController) {
        SceneNavigator.mainController = mainController;
    }

    /**
     *
     * @param fxml
     */
    public static void loadScene(String fxml) {
        try {
            mainController.setScene(FXMLLoader.load(ClassLoader.getSystemResource(fxml)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}