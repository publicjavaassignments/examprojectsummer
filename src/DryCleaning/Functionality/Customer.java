package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Customer {

    private long customerId;
    private String phoneNumber;
    private String email;
    private String name;

    /**
     * Return customer Id
     * @return
     */
    public long getCustomerId() {
        return customerId;
    }

    /**
     * return customer phone number
     * @return
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * return customer email
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Return customer name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Create a new customer
     * @param customerId
     * @param name
     * @param email
     * @param phoneNumber
     * @throws SQLException
     */
    public static void createCustomer(long customerId, String name, String email, String phoneNumber) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        SQLServer.statement.executeUpdate("INSERT INTO tblCustomer " +
                "VALUES (" + customerId + ", '" + phoneNumber + "', '" + email + "', '" + name + "');");
    }

    /**
     * Select all data about the customer from the database
     * @param customerId
     * @throws SQLException
     */
    public void getDetails(long customerId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblCustomer WHERE CustomerID = " + customerId);

        while (rs.next()) {
            this.customerId = rs.getLong("CustomerID");
            phoneNumber = rs.getString("PhoneNumber");
            email = rs.getString("Email");
            name = rs.getString("Name");
        }
        SQLServer.statement.close();
    }

    /**
     * Select customer id from the database.
     * @param input
     * @return
     * @throws SQLException
     */
    public long getCustomerId(String input) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        long customerId = 0;
        ResultSet rs = SQLServer.statement.executeQuery("SELECT CustomerID FROM tblCustomer " +
                "WHERE PhoneNumber = '" + input + "' OR Email = '" + input + "';");
        while (rs.next()) {
            customerId = rs.getLong("CustomerID");
        }
        SQLServer.statement.close();
        return customerId;
    }
}