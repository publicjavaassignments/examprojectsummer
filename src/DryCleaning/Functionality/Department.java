package DryCleaning.Functionality;

import DryCleaning.Database.HandleID;
import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Department {

    private long id;
    private String address;
    private String name;
    private String email;
    private String phoneNumber;


    public Department() {

    }

    public Department(long id, String address, String name, String email, String phoneNumber) {
        this.id = id;
        this.address = address;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    /**
     * return id.
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * Return address.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Return name.
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Return email.
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * return phone number.
     * @return
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }


    public static void createDepartment(String address, String name, String email, String phoneNumber, String databaseTable) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        long id = HandleID.getUniqueId();
        SQLServer.statement.executeUpdate("INSERT INTO " + databaseTable +
                " VALUES (" + id + ", '" + address + "', '" + name + "', '" + email + "', '" + phoneNumber + "');");
        SQLServer.statement.close();
    }

    /**
     * Select all data from the database about the department.
     * @param id
     * @throws SQLException
     */
    public void getDetails(long id, String databaseTable) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs;

        if(databaseTable.equals("tblDeliveryPoint")) {
            rs = SQLServer.statement.executeQuery("SELECT * FROM tblDeliveryPoint WHERE DeliveryPointID = " + id);
            while (rs.next()) {
                this.id = rs.getLong(1);
                this.address = rs.getString("Address");
                this.name = rs.getString("Name");
                this.email = rs.getString("Email");
                this.phoneNumber = rs.getString("PhoneNumber");
            }
        } else if (databaseTable.equals("tblCentral")) {
            rs = SQLServer.statement.executeQuery("SELECT * FROM tblCentral WHERE CentralID = " + id);
            while (rs.next()) {
                this.id = rs.getLong(1);
                this.address = rs.getString("Address");
                this.name = rs.getString("Name");
                this.email = rs.getString("Email");
                this.phoneNumber = rs.getString("PhoneNumber");
            }
        }
        SQLServer.statement.close();
    }

}
