package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TransportContent {

    private long transportId;
    private long orderId;

    /**
     * Insert all orders in the transport.
     * @param transportId
     * @param orderIdList
     * @throws SQLException
     */
    public void createContent(long transportId, ArrayList<Long> orderIdList) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        for (long orderId : orderIdList) {
            SQLServer.statement.executeUpdate("INSERT INTO tblTransportContent VALUES (" + transportId + ", " + orderId + ");");
        }
        SQLServer.statement.close();
    }

    /**
     * Return all orders in the current transport.
     * @param transportId
     * @return
     * @throws SQLException
     */
    public ArrayList<Long> populateOrderList(long transportId) throws SQLException {
        ArrayList<Long> orderIdList = new ArrayList<>();
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblTransportContent WHERE TransportID = " + transportId);

        while (rs.next()) {
            orderIdList.add(rs.getLong("OrderID"));
        }
        SQLServer.statement.close();
        return orderIdList;
    }
}