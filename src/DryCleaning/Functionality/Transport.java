package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class Transport {

    private long transportId;
    private long driverId;
    private Date date;
    private int status;
    private boolean isDone;

    /**
     * Create a new transport.
     * @param transportId
     * @param driverId
     * @param status
     * @param isDone
     * @throws SQLException
     */
    public static void createTransport(long transportId, long driverId, int status, boolean isDone) throws SQLException {
        Timestamp date = new Timestamp(System.currentTimeMillis());

        SQLServer.statement = SQLServer.connect.createStatement();
        SQLServer.statement.executeUpdate("INSERT INTO tblTransport VALUES (" + transportId + ", " + driverId + ", '" + date + "', " + status + ", '" + isDone + "');");
        SQLServer.statement.close();
    }

    /**
     * Set the transport status to done.
     * @param transportId
     * @throws SQLException
     */
    public static void setDone(long transportId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        SQLServer.statement.executeUpdate("UPDATE tblTransport SET isDone = 'true' WHERE TransportID = " + transportId);
        SQLServer.statement.close();
    }

    /**
     * Get the most recent transport made by the driver.
     * @param driverId
     * @return
     * @throws SQLException
     */
    public boolean getNewestTransportStatus(long driverId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT TransportID, isDone FROM tblTransport WHERE Date IN (SELECT max(Date) FROM tblTransport WHERE DriverID = " + driverId + ");");

        if (!rs.isBeforeFirst()) {
            return true;
        } else {
            while (rs.next()) {
                isDone = rs.getBoolean("isDone");
                this.transportId = rs.getLong("TransportID");
            }
        }
        SQLServer.statement.close();
        return isDone;
    }

    /**
     * Return transport id
     * @return
     */
    public long getTransportId() {
        return transportId;
    }
}