package DryCleaning.Functionality;

import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.AnimateFXInterpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.util.Duration;

public class Animations {

    /**
     * Create fadeout animation
     * @param scene
     * @param root
     */
    public static void fadeOut(String scene, Node root) {
        Timeline fadeOut = new Timeline(
                new KeyFrame((Duration.millis(0)),
                        new KeyValue(root.opacityProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(root.scaleXProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(root.scaleYProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(root.scaleZProperty(), 0.3, AnimateFXInterpolator.EASE)),
                new KeyFrame(Duration.millis(250),
                        new KeyValue(root.opacityProperty(), 0, AnimateFXInterpolator.EASE),
                        new KeyValue(root.scaleXProperty(), 0.3, AnimateFXInterpolator.EASE),
                        new KeyValue(root.scaleYProperty(), 0.3, AnimateFXInterpolator.EASE),
                        new KeyValue(root.scaleZProperty(), 0.3, AnimateFXInterpolator.EASE)),
                new KeyFrame(Duration.millis(500),
                        new KeyValue(root.opacityProperty(), 0, AnimateFXInterpolator.EASE)));
        fadeOut.setOnFinished(e -> SceneNavigator.loadScene(scene));

        fadeOut.play();
    }
}
