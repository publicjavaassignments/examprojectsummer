package DryCleaning.Functionality;

import DryCleaning.Database.HandleID;
import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLOutput;

public class Category {

    private static long categoryId;
    private static String name;

    /**
     * Creates a new category and insert it into the database.
     * @param categoryName
     * @throws SQLException
     */
    public static void createCategory(String categoryName) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        long categoryId = HandleID.getUniqueId();
        SQLServer.statement.executeUpdate("EXEC dbo.insertCategory @CategoryID = " + categoryId + ", @Name = '" + categoryName + "';");
        SQLServer.statement.close();
    }

    /**
     * Fetch categoryID from the database.
     * @param categoryName
     * @return
     * @throws SQLException
     */
    public long getCategoryId(String categoryName) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT CategoryID FROM tblLaundryCategory WHERE Name = '" + categoryName + "';");
        while (rs.next()) {
            categoryId = rs.getLong("CategoryID");
        }
        SQLServer.statement.close();
        return categoryId;
    }

    /**
     * Fetch category name from the database.
     * @param categoryId
     * @return
     * @throws SQLException
     */
    public String getName(long categoryId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT Name FROM tblLaundryCategory WHERE CategoryID = " + categoryId + ";");
        while (rs.next()) {
            name = rs.getString("Name");
        }
        SQLServer.statement.close();
        return name;
    }
}