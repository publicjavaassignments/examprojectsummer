package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Order {

    private long orderId;
    private Date date;
    private long centralId;
    private int status;
    private long deliveryPointId;
    private long customerId;
    private String readableId;

    public Order() {

    }

    public Order(long orderId, Date date, long centralId, int status, long deliveryPointId, long customerId, String readableId) {
        this.orderId = orderId;
        this.date = date;
        this.centralId = centralId;
        this.status = status;
        this.deliveryPointId = deliveryPointId;
        this.customerId = customerId;
        this.readableId = readableId;
    }

    /**
     * return order delivery point id
     *
     * @return
     */
    public long getDeliveryPointId() {
        return deliveryPointId;
    }

    /**
     * Return order customer id
     *
     * @return
     */
    public long getCustomerId() {
        return customerId;
    }

    /**
     * Return order Readable id
     *
     * @return
     */
    public String getReadableId() {
        return readableId;
    }

    /**
     * Return order id.
     *
     * @return
     */
    public long getOrderId() {
        return orderId;
    }

    /**
     * Return order date
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     * return order central id
     *
     * @return
     */
    public long getCentralId() {
        return centralId;
    }

    /**
     * Return order status
     *
     * @return
     */
    public int getStatus() {
        return status;
    }


    /**
     * Insert the order into the database.
     *
     * @param orderId
     * @param deliveryPointId
     * @param customerId
     * @param centralId
     * @param readableId
     * @throws SQLException
     */
    public static void createOrder(long orderId, long deliveryPointId, long customerId, long centralId, String readableId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        Timestamp date = new Timestamp(System.currentTimeMillis());

        //status 1 is first stage of the cleaning flow, when the delivery point creates the order.
        int status = 1;

        SQLServer.statement.executeUpdate("INSERT INTO tblOrder " +
                "VALUES (" + orderId + ", '" + date + "', " + centralId + ", " + status + ", " + deliveryPointId + ", " + customerId + ", '" + readableId + "');");
        SQLServer.statement.close();
    }

    /**
     * Select all orders from the database.
     *
     * @return
     * @throws SQLException
     */
    public static ArrayList<Order> getOrders() throws SQLException {
        ArrayList<Order> ordersList = new ArrayList<>();

        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblOrder");

        while (rs.next()) {
            ordersList.add(new Order(rs.getLong("OrderID"),
                    rs.getDate("Date"),
                    rs.getLong("CentralID"),
                    rs.getInt("Status"),
                    rs.getLong("DeliveryPointID"),
                    rs.getLong("CustomerID"),
                    rs.getString("ReadableID")));
        }
        SQLServer.statement.close();
        return ordersList;
    }

    /**
     * Call the stored procedure SetStatus, to increment the order status.
     *
     * @param orderId
     * @throws SQLException
     */
    public static void setStatus(long orderId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        SQLServer.statement.executeUpdate("EXEC dbo.SetStatus @OrderID = " + orderId);
        SQLServer.statement.close();
    }

    /**
     * Select all data from the database about the order with the order id.
     *
     * @param id
     * @throws SQLException
     */
    public void getDetails(long id) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblOrder WHERE OrderID = " + id);
        while (rs.next()) {
            orderId = rs.getLong("OrderID");
            date = rs.getDate("Date");
            centralId = rs.getLong("CentralID");
            status = rs.getInt("Status");
            deliveryPointId = rs.getLong("DeliveryPointID");
            customerId = rs.getLong("CustomerID");
            readableId = rs.getString("ReadableID");
        }
        SQLServer.statement.close();
    }

    /**
     * Select all data from the database about the order with the readable order id.
     *
     * @param id
     * @throws SQLException
     */
    public void getDetails(String id) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblOrder WHERE ReadableID = " + id);
        while (rs.next()) {
            orderId = rs.getLong("OrderID");
            date = rs.getDate("Date");
            centralId = rs.getLong("CentralID");
            status = rs.getInt("Status");
            deliveryPointId = rs.getLong("DeliveryPointID");
            customerId = rs.getLong("CustomerID");
            readableId = rs.getString("ReadableID");
        }
        SQLServer.statement.close();
    }
}