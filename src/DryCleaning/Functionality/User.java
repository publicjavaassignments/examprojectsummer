package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
    private String username;
    private String password;
    private long deliveryPointId;
    private long centralId;
    private long driverId;
    private boolean permDeliveryPoint;
    private boolean permCentral;
    private boolean permDriver;
    private boolean permAdmin;

    static User user = new User();

    private User() {
    }

    public User(String username, String password, long deliveryPointId, long centralId, long driverId, boolean permDeliveryPoint, boolean permCentral, boolean permDriver, boolean permAdmin) {
        this.username = username;
        this.password = password;
        this.deliveryPointId = deliveryPointId;
        this.centralId = centralId;
        this.driverId = driverId;
        this.permDeliveryPoint = permDeliveryPoint;
        this.permCentral = permCentral;
        this.permDriver = permDriver;
        this.permAdmin = permAdmin;
    }

    /**
     * User singleton
     *
     * @return
     */
    public static User getInstance() {
        return user;
    }

    /**
     * Return user
     * @return
     */
    public static User getUser() {
        return user;
    }

    /**
     * Return username
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * Return password
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * return user's delivery point id
     *
     * @return
     */
    public long getDeliveryPointId() {
        return deliveryPointId;
    }

    /**
     * return user's central id
     *
     * @return
     */
    public long getCentralId() {
        return centralId;
    }

    /**
     * return user's driver id.
     *
     * @return
     */
    public long getDriverId() {
        return driverId;
    }

    /**
     * return user's delivery point permission
     *
     * @return
     */
    public boolean getPermDeliveryPoint() {
        return permDeliveryPoint;
    }

    /**
     * return user's central permission
     *
     * @return
     */
    public boolean getPermCentral() {
        return permCentral;
    }

    /**
     * return user's driver permission
     *
     * @return
     */
    public boolean getPermDriver() {
        return permDriver;
    }

    /**
     * return user's admin permission
     *
     * @return
     */
    public boolean getPermAdmin() {
        return permAdmin;
    }

    /**
     * Insert a new user into the database.
     *
     * @param username
     * @param password
     * @param deliveryPointId
     * @param centralId
     * @param driverId
     * @param permDeliveryPoint
     * @param permCentral
     * @param permDriver
     * @throws SQLException
     */
    public static void createUser(String username, String password, long deliveryPointId, long centralId, long driverId,
                                     boolean permDeliveryPoint, boolean permCentral, boolean permDriver, boolean permAdmin) throws SQLException {

        SQLServer.statement = SQLServer.connect.createStatement();
        SQLServer.statement.executeUpdate("INSERT INTO tblUser VALUES ('" + username +
                "', '" + password +
                "', " + deliveryPointId +
                ", " + centralId +
                ", " + driverId +
                ", '" + permDeliveryPoint +
                "', '" + permCentral +
                "', '" + permDriver +
                "', '" + permAdmin + "');");

        SQLServer.statement.close();
    }

    /**
     * Update existing user's data
     *
     * @param username
     * @param password
     * @param deliveryPointId
     * @param centralId
     * @param driverId
     * @param permDeliveryPoint
     * @param permCentral
     * @param permDriver
     * @param permAdmin
     * @throws SQLException
     */
    public static void updateUser(String username, String password, long deliveryPointId, long centralId, long driverId,
                                  boolean permDeliveryPoint, boolean permCentral, boolean permDriver, boolean permAdmin) throws SQLException {

        SQLServer.statement = SQLServer.connect.createStatement();
        SQLServer.statement.executeUpdate("UPDATE tblUser SET Password = '" + password +
                "', DeliveryPointID = " + deliveryPointId +
                ", CentralID = " + centralId + ", DriverID = " + driverId +
                ", PermDeliveryPoint = '" + permDeliveryPoint +
                "', PermCentral = '" + permCentral +
                "', PermDriver = '" + permDriver +
                "', PermAdmin = '" + permAdmin + "'" +
                "WHERE Username = '" + username + "';");
        SQLServer.statement.close();
    }

    /**
     * Check if the user data matches the entered credentials.
     *
     * @param userName
     * @param password
     * @return
     * @throws SQLException
     */
    public boolean login(String userName, String password) throws SQLException {
        try {
            getUserData(userName, password);

            if (this.username.equals(userName) && this.password.equals(password)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select all user data from the database.
     *
     * @param userName
     * @param password
     * @throws SQLException
     */
    public void getUserData(String userName, String password) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblUser WHERE Username = '" + userName + "' AND Password = '" + password + "';");

        while (rs.next()) {
            this.username = rs.getString("Username");
            this.password = rs.getString("Password");
            this.deliveryPointId = rs.getLong("DeliveryPointID");
            this.centralId = rs.getLong("CentralID");
            this.driverId = rs.getLong("DriverID");
            this.permDeliveryPoint = rs.getBoolean("PermDeliveryPoint");
            this.permCentral = rs.getBoolean("PermCentral");
            this.permDriver = rs.getBoolean("PermDriver");
            this.permAdmin = rs.getBoolean("PermAdmin");
        }
        SQLServer.statement.close();
    }

    /**
     * Search user information
     * @param userName
     * @return
     * @throws SQLException
     */
    public static User searchUser(String userName) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblUser WHERE Username = '" + userName + "';");

        while (rs.next()) {
            return new User(rs.getString("Username"),
                    rs.getString("Password"),
                    rs.getLong("DeliveryPointID"),
                    rs.getLong("CentralID"),
                    rs.getLong("DriverID"),
                    rs.getBoolean("PermDeliveryPoint"),
                    rs.getBoolean("PermCentral"),
                    rs.getBoolean("PermDriver"),
                    rs.getBoolean("PermAdmin"));

        }
        SQLServer.statement.close();
        return null;
    }

    /**
     * Reset user when logged out.
     */
    public void reset() {
        user = new User();
    }

    /**
     * Hash the entered password
     *
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}