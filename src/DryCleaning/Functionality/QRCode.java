package DryCleaning.Functionality;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;

public class QRCode {

    /**
     * Creates a QR code
     * @param orderNumber
     * @param width
     * @param height
     * @param filePath
     * @throws WriterException
     * @throws IOException
     */
    public static void generateOrderQRCode(String orderNumber, int width, int height, String filePath)
            throws WriterException, IOException {

        QRCodeWriter QRWriter = new QRCodeWriter();
        BitMatrix BitTransformMatrix = QRWriter.encode(orderNumber, BarcodeFormat.QR_CODE, width, height);

        Path FilePath = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(BitTransformMatrix, "PNG", FilePath);
    }

    /**
     * Read a QRCode
     * @param file
     * @return
     * @throws IOException
     * @throws FormatException
     * @throws ChecksumException
     * @throws NotFoundException
     */
    public static String read(File file) throws IOException, FormatException, ChecksumException, NotFoundException {
        QRCodeReader reader = new QRCodeReader();
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new BufferedImageLuminanceSource(
                        ImageIO.read(new FileInputStream(file)))));
        Result result = reader.decode(binaryBitmap);

        return result.getText();
    }

    /**
     *
     * @param toWrite
     * @return
     */
    public static ImageView generateQRCodeImageView (String toWrite) {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        String myWeb = toWrite;
        int width = 200;
        int height = 200;

        BufferedImage bufferedImage = null;
        try {
            BitMatrix byteMatrix = qrCodeWriter.encode(myWeb, BarcodeFormat.QR_CODE, width, height);
            bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            bufferedImage.createGraphics();

            Graphics2D graphics = (Graphics2D) bufferedImage.getGraphics();
            Color background = new Color(39, 39, 39);
            graphics.setColor(background); // bg
            graphics.fillRect(0, 0, width, height);
            Color foreground = new Color(255, 140, 0);
            graphics.setColor(foreground); // QR code

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (byteMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }

        } catch (WriterException e) {
            e.printStackTrace();
        }

        ImageView qrView = new ImageView();
        qrView.setImage(SwingFXUtils.toFXImage(bufferedImage, null));

        return qrView;
    }
}