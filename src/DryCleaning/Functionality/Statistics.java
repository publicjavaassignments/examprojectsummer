package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;
import javafx.scene.chart.XYChart;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Statistics {

    /**
     * Select all orders from the database, and return a series.
     * @return
     * @throws SQLException
     */
    public static XYChart.Series getStatistics() throws SQLException {
        ArrayList<Date> dateList = new ArrayList<>();
        XYChart.Series<String, Integer> series = new XYChart.Series<>();

        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT Date FROM tblOrder");

        while (rs.next()) {
            Date date = rs.getDate("Date");
            dateList.add(date);
        }

        for (Date date : dateList) {
            series.getData().add(new XYChart.Data<>(date.toString(), Collections.frequency(dateList, date)));
        }
        SQLServer.statement.close();
        return series;
    }
}
