package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DeliveryPoint extends Department{

    public DeliveryPoint() {

    }

    public DeliveryPoint(long deliveryPointId, String address, String name, String email, String phoneNumber) {
        super(deliveryPointId, address, name, email, phoneNumber);
    }

    /**
     * @param id
     * @throws SQLException
     */
    public void getDetails(long id) throws SQLException {
        super.getDetails(id, "tblDeliveryPoint");
    }

    /**
     * Return all delivery points
     * @return
     * @throws SQLException
     */
    public static ArrayList<DeliveryPoint> getDeliveryPoints() throws SQLException {
        ArrayList<DeliveryPoint> deliveryPointList = new ArrayList<>();
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblDeliveryPoint");

        while (rs.next()) {
            deliveryPointList.add(new DeliveryPoint(rs.getLong("DeliveryPointID"), rs.getString("Address"),
                    rs.getString("Name"), rs.getString("Email"), rs.getString("PhoneNumber")));
        }
        SQLServer.statement.close();
        return deliveryPointList;
    }
}