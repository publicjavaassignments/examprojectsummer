package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Central extends Department {

    public Central() {

    }

    public Central(long centralId, String address, String name, String email, String phoneNumber) {
        super(centralId, address, name, email, phoneNumber);
    }

    /**
     * @param id
     * @throws SQLException
     */
    public void getDetails(long id) throws SQLException {
        super.getDetails(id, "tblCentral");
    }


    /**
     * Return all centrals
     * @return
     * @throws SQLException
     */
    public static ArrayList<Central> getCentrals() throws SQLException {
        ArrayList<Central> centralList = new ArrayList<>();

        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblCentral");

        while (rs.next()) {
            centralList.add(new Central(rs.getLong("CentralID"),
                    rs.getString("Address"),
                    rs.getString("Name"),
                    rs.getString("Email"),
                    rs.getString("PhoneNumber")));
        }
        return centralList;
    }

}