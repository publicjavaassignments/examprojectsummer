package DryCleaning.Functionality;

import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OrderContent {

    private long orderId;
    private ArrayList<Long> categoryIdList = new ArrayList<>();
    private ArrayList<Integer> amountList = new ArrayList<>();

    /**
     * Return order id
     * @return
     */
    public long getOrderId() {
        return orderId;
    }

    /**
     * return order categories.
     * @return
     */
    public ArrayList<Long> getCategoryIdList() {
        return categoryIdList;
    }

    /**
     * return amount of different categories.
     * @return
     */
    public ArrayList<Integer> getAmountList() {
        return amountList;
    }

    /**
     * Insert the order's content into the database.
     * @param orderId
     * @param categoryIdList
     * @param amountList
     * @throws SQLException
     */
    public static void createContent(long orderId, ArrayList<Long> categoryIdList, ArrayList<Integer> amountList) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();

        for (int i = 0; i < categoryIdList.size(); i++) {
            if(categoryIdList.get(i) > 0 && amountList.get(i) > 0) {
                SQLServer.statement.executeUpdate("INSERT INTO tblOrderContent VALUES (" + orderId + ", " + categoryIdList.get(i) + ", " + amountList.get(i) + ");");
            }
        }
        SQLServer.statement.close();
    }

    /**
     * Select all data from the database about the order, and populate the arraylists.
     * @param orderId
     * @throws SQLException
     */
    public void getDetails(long orderId) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblOrderContent WHERE OrderID = " + orderId);

        while (rs.next()) {
            this.orderId = rs.getLong("OrderID");
            categoryIdList.add(rs.getLong("CategoryID"));
            amountList.add(rs.getInt("Amount"));
        }
        SQLServer.statement.close();
    }
}