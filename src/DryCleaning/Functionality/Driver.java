package DryCleaning.Functionality;

import DryCleaning.Database.HandleID;
import DryCleaning.Database.SQLServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Driver {

    private long driverId;
    private String name;
    private String phoneNumber;
    private String email;

    public Driver() {

    }

    public Driver(long driverId, String name, String phoneNumber, String email) {
        this.driverId = driverId;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    /**
     * Return driver id
     * @return
     */
    public long getDriverId() {
        return driverId;
    }

    /**
     * Return driver name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Return driver phone
     * @return
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Return driver email
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Create a new driver.
     * @param name
     * @param phoneNumber
     * @param email
     * @throws SQLException
     */
    public static void createDriver(String name, String phoneNumber, String email) throws SQLException {
        SQLServer.statement = SQLServer.connect.createStatement();
        long driverId = HandleID.getUniqueId();

        SQLServer.statement.executeUpdate("INSERT INTO tblDriver VALUES (" + driverId + ", '" + name + "', '" + phoneNumber + "', '" + email + "');");
        SQLServer.statement.close();
    }

    /**
     * Select all data about the driver
     * @param driverId
     * @throws SQLException
     */
    public void getDetails(long driverId) throws SQLException {

        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblDriver WHERE DriverID = " + driverId);

        while (rs.next()) {
            this.driverId = rs.getLong("DriverID");
            this.name = rs.getString("Name");
            this.email = rs.getString("Email");
            this.phoneNumber = rs.getString("PhoneNumber");
        }
        SQLServer.statement.close();

    }

    /**
     * Return all drivers
     * @return
     * @throws SQLException
     */
    public static ArrayList<Driver> getDrivers() throws SQLException {
        ArrayList<Driver> driverList = new ArrayList<>();
        SQLServer.statement = SQLServer.connect.createStatement();
        ResultSet rs = SQLServer.statement.executeQuery("SELECT * FROM tblDriver");

        while (rs.next()) {
            driverList.add(new Driver(rs.getLong("DriverID"), rs.getString("Name"), rs.getString("Email"), rs.getString("PhoneNumber")));
        }
        SQLServer.statement.close();
        return driverList;
    }

}
