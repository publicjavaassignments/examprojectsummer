package DryCleaning.Application.Central;

import DryCleaning.Database.HandleID;
import DryCleaning.Functionality.Animations;
import DryCleaning.Functionality.Order;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.ZoomIn;
import com.jfoenix.controls.JFXListView;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CentralMenuCentralController {
    @FXML
    private JFXListView listViewUpcoming, listViewCurrent, listViewDone;
    @FXML
    private StackPane root;
    @FXML
    private BorderPane BorderPaneAnimations;

    private long selectedOrder;

    public void initialize() throws SQLException {
        BorderPaneAnimations.setOpacity(0);
        populateOrders();
        new ZoomIn(BorderPaneAnimations).play();
    }

    /**
     * Populate the three listviews with orders.
     * @throws SQLException
     */
    public void populateOrders() throws SQLException {
        listViewUpcoming.getItems().clear();
        listViewCurrent.getItems().clear();
        listViewDone.getItems().clear();
        ArrayList<Order> orderList = new ArrayList<>();
        orderList.clear();
        orderList = Order.getOrders();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-YYYY");

        //3 == arrived, 4 == ongoing, 5 == finished.
        for (Order order : orderList) {
            if (order.getStatus() == 3) {
                listViewUpcoming.getItems().addAll(order.getOrderId() + " - " + format.format(HandleID.getDate(order.getOrderId())));
            } else if (order.getStatus() == 4) {
                listViewCurrent.getItems().addAll(order.getOrderId() + " - " + format.format(HandleID.getDate(order.getOrderId())));
            } else if (order.getStatus() == 5) {
                listViewDone.getItems().addAll(order.getOrderId() + " - " + format.format(HandleID.getDate(order.getOrderId())));
            }
        }
    }

    /**
     * Move the order to next stage.
     * @throws SQLException
     */
    public void moveOrder() throws SQLException {
        Order.setStatus(selectedOrder);
        populateOrders();
    }

    /**
     * Get the selected order from upcoming listview.
     */
    public void getSelectedUpcomingOrder() {
        selectedOrder = Long.parseLong(listViewUpcoming.getSelectionModel().getSelectedItem().toString().substring(0, 19));
    }

    /**
     * Get the selected order from current listview
     */
    public void getSelectedCurrentOrder() {
        selectedOrder = Long.parseLong(listViewCurrent.getSelectionModel().getSelectedItem().toString().substring(0, 19));
    }

    /**
     * Navigate to the previous menu.
     */
    public void handleBackButton() {
        Animations.fadeOut(SceneNavigator.CENTRAL_PRELIMINARY, root);

    }
}