package DryCleaning.Application.Central;

import DryCleaning.Database.HandleID;
import DryCleaning.Functionality.Animations;
import DryCleaning.Functionality.Order;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.ZoomIn;
import com.jfoenix.controls.JFXListView;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class CentralMenuUpcomingController {

    @FXML
    private StackPane root;

    @FXML
    private BorderPane BorderPaneAnimations;

    @FXML
    private JFXListView listViewShop, listViewUpcoming;

    public void initialize() throws SQLException {
        BorderPaneAnimations.setOpacity(0);
        populateOrders();
        new ZoomIn(BorderPaneAnimations).play();
    }

    /**
     * Populate the two listviews with orders.
     * @throws SQLException
     */
    public void populateOrders() throws SQLException {
        listViewUpcoming.getItems().clear();
        listViewShop.getItems().clear();
        ArrayList<Order> orderList = new ArrayList<>();
        orderList.clear();
        orderList = Order.getOrders();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-YYYY");

        //1 == at the delivery point, 2 == one the way to the central.
        for (Order order : orderList) {
            if (order.getStatus() == 1) {
                listViewShop.getItems().addAll(order.getOrderId() + " - " + format.format(HandleID.getDate(order.getOrderId())));
            } else if (order.getStatus() == 2) {
                listViewUpcoming.getItems().addAll(order.getOrderId() + " - " + format.format(HandleID.getDate(order.getOrderId())));
            }
        }
    }

    /**
     * Navigate to the previous menu.
     */
    public void handleBackButton() {
        Animations.fadeOut(SceneNavigator.CENTRAL_PRELIMINARY, root);
    }



}
