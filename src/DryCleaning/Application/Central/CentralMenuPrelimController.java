package DryCleaning.Application.Central;

import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.FadeInUp;
import Libs.animatefx.animation.FadeOut;
import com.jfoenix.controls.JFXButton;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

import static javafx.animation.Interpolator.EASE_BOTH;

public class CentralMenuPrelimController {

    public static String buttonClicked;
    @FXML
    private StackPane root;
    @FXML
    private Text centralText, upcomingText;
    @FXML
    private JFXButton backButtonPrelim, btnCentral, btnUpcoming;
    private boolean hasSelected = false;

    public void initialize() {
        btnCentral.setOpacity(0);
        btnUpcoming.setOpacity(0);
        centralText.setOpacity(0);
        upcomingText.setOpacity(0);
        backButtonPrelim.setOpacity(0);

        btnCentral.setOnMouseExited(t -> handleHoverAnimationShop(btnCentral, 1));
        btnCentral.setOnMouseEntered(t -> handleHoverAnimationShop(btnCentral, 0));
        btnCentral.setOnMousePressed(t -> {
            buttonClicked = "central";
            handleClickAnimationShop(btnCentral, 0);
        });

        btnUpcoming.setOnMouseExited(t -> handleHoverAnimationShop(btnUpcoming, 1));
        btnUpcoming.setOnMouseEntered(t -> handleHoverAnimationShop(btnUpcoming, 0));
        btnUpcoming.setOnMousePressed(t -> {
            buttonClicked = "driver";
            handleClickAnimationShop(btnUpcoming, 1);
        });

        Timeline fadeGUI = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(backButtonPrelim.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(centralText.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(upcomingText.opacityProperty(),
                        1,
                        EASE_BOTH)));

        Thread animationThread = new Thread(() -> {
            try {
                Thread.sleep(300);
                assignAnimationShop(btnCentral);
                Thread.sleep(200);
                assignAnimationShop(btnUpcoming);
                Thread.sleep(500);
                fadeGUI.play();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        animationThread.start();
    }

    /**
     * Navigate to the main menu
     */
    public void handleBackButton() {

        Timeline buttonClick = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(root.scaleXProperty(),
                        0.25,
                        EASE_BOTH),
                new KeyValue(root.opacityProperty(),
                        0,
                        EASE_BOTH),
                new KeyValue(root.scaleYProperty(),
                        0.25,
                        EASE_BOTH)));
        buttonClick.play();
        buttonClick.setOnFinished(e -> SceneNavigator.loadScene(SceneNavigator.MainMenu));

    }

    /**
     * Navigate to either Central menu or central upcoming
     * @param button
     * @param clickedButton
     */
    private void handleClickAnimationShop(JFXButton button, int clickedButton) {
        hasSelected = true;

        Timeline buttonClick = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(button.scaleXProperty(),
                        1.75,
                        EASE_BOTH),
                new KeyValue(button.scaleYProperty(),
                        1.75,
                        EASE_BOTH)));

        switch (clickedButton) {
            case 0: // btnCentral
                buttonClick.play();
                new FadeOut(btnUpcoming).play();
                new FadeOut(btnCentral).play();
                new FadeOut(centralText).play();
                new FadeOut(upcomingText).play();
                new FadeOut(backButtonPrelim).play();
                buttonClick.setOnFinished(e -> SceneNavigator.loadScene(SceneNavigator.CENTRAL_MENU));
                break;
            case 1: // btnUpcoming
                buttonClick.play();
                new FadeOut(btnUpcoming).play();
                new FadeOut(btnCentral).play();
                new FadeOut(centralText).play();
                new FadeOut(upcomingText).play();
                new FadeOut(backButtonPrelim).play();
                buttonClick.setOnFinished(e -> SceneNavigator.loadScene(SceneNavigator.CENTRAL_MENU_UPCOMING));
                break;
            default:
                break;
        }
    }

    /**
     * Create fadeinup animation
     * @param button
     */
    private void assignAnimationShop(JFXButton button) {
        new FadeInUp(button).play();
    }

    /**
     * Animation when hovered over
     * @param button
     * @param buttonStatus
     */
    private void handleHoverAnimationShop(JFXButton button, int buttonStatus) {
        if (hasSelected == true) {
            // Nothing!
        } else {
            Timeline buttonHover = new Timeline(new KeyFrame(Duration.millis(75),
                    new KeyValue(button.scaleXProperty(),
                            1.1,
                            EASE_BOTH),
                    new KeyValue(button.scaleYProperty(),
                            1.1,
                            EASE_BOTH)));

            Timeline buttonUnhover = new Timeline(new KeyFrame(Duration.millis(75),
                    new KeyValue(button.scaleXProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(button.scaleYProperty(),
                            1,
                            EASE_BOTH)));

            if (buttonStatus == 0) {
                buttonHover.play();
            } else if (buttonStatus == 1) {
                buttonUnhover.play();
            }
        }
    }

}
