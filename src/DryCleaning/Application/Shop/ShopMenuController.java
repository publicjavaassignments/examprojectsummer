package DryCleaning.Application.Shop;

import DryCleaning.Application.Main.LoginPromptController;
import DryCleaning.Database.HandleID;
import DryCleaning.Functionality.*;
import DryCleaning.Functionality.QRCode;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.FadeIn;
import com.google.zxing.WriterException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class ShopMenuController {
    @FXML
    private BorderPane rootPane;
    @FXML
    private StackPane root;

    private Category category;

    @FXML
    private JFXTextField txfName, txfEmail, txfPhone;
    @FXML
    private Text habitAmount, frakkeAmount, kjoleAmount, dragtAmount, cottonCoatAmount, vindjakkeAmount, bluseAmount,
            nederdelAmount, jakkeAmount, benklaederAmount, troejeAmount, taeppeAmount, gardinerAmount;
    @FXML
    private ToggleButton btnHabit, btnFrakke, btnKjole, btnDragt, btnCottonCoat, btnVindjakke, btnBluse, btnNederdel,
            btnJakke, btnBenklaeder, btnTroeje, btnTaeppe, btnGardiner;
    @FXML
    private JFXButton btnHabitPlus, btnFrakkePlus, btnKjolePlus, btnDragtPlus, btnCottonCoatPlus, btnVindjakkePlus,
            btnBlusePlus, btnNederdelPlus, btnJakkePlus, btnBenklaederPlus, btnTroejePlus, btnTaeppePlus, btnGardinerPlus;
    @FXML
    private JFXButton btnHabitMinus, btnFrakkeMinus, btnKjoleMinus, btnDragtMinus, btnCottonCoatMinus, btnVindjakkeMinus,
            btnBluseMinus, btnNederdelMinus, btnJakkeMinus, btnBenklaederMinus, btnTroejeMinus, btnTaeppeMinus, btnGardinerMinus;


    private ArrayList<Text> amountTextList = new ArrayList<>();
    private ArrayList<Long> categoryList = new ArrayList<>();
    private ArrayList<Integer> amountList = new ArrayList<>();

    public void initialize() {

        //Populate the arraylists with default values.
        for (int i = 0; i < 13; i++) {
            categoryList.add(0L);
        }
        for (int i = 0; i < 13; i++) {
            amountList.add(0);
        }

        amountTextList.add(habitAmount);
        amountTextList.add(frakkeAmount);
        amountTextList.add(kjoleAmount);
        amountTextList.add(dragtAmount);
        amountTextList.add(cottonCoatAmount);
        amountTextList.add(vindjakkeAmount);
        amountTextList.add(bluseAmount);
        amountTextList.add(nederdelAmount);
        amountTextList.add(jakkeAmount);
        amountTextList.add(benklaederAmount);
        amountTextList.add(troejeAmount);
        amountTextList.add(taeppeAmount);
        amountTextList.add(gardinerAmount);

        rootPane.setOpacity(0);
        Thread animationThread = new Thread(() -> {
            try {
                Thread.sleep(300);
                new FadeIn(rootPane).play();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        animationThread.start();
    }

    /**
     * Set the amount Text when the amount is incremented.
     * @param e
     */
    public void addAmount(ActionEvent e) {
        JFXButton button = (JFXButton) e.getSource();
        if (button.equals(btnHabitPlus)) {
            habitAmount.setText("" + (Integer.parseInt(habitAmount.getText()) + 1));
        } else if (button.equals(btnFrakkePlus)) {
            frakkeAmount.setText("" + (Integer.parseInt(frakkeAmount.getText()) + 1));
        } else if (button.equals(btnKjolePlus)) {
            kjoleAmount.setText("" + (Integer.parseInt(kjoleAmount.getText()) + 1));
        } else if (button.equals(btnDragtPlus)) {
            dragtAmount.setText("" + (Integer.parseInt(dragtAmount.getText()) + 1));
        } else if (button.equals(btnCottonCoatPlus)) {
            cottonCoatAmount.setText("" + (Integer.parseInt(cottonCoatAmount.getText()) + 1));
        } else if (button.equals(btnVindjakkePlus)) {
            vindjakkeAmount.setText("" + (Integer.parseInt(vindjakkeAmount.getText()) + 1));
        } else if (button.equals(btnBlusePlus)) {
            bluseAmount.setText("" + (Integer.parseInt(bluseAmount.getText()) + 1));
        } else if (button.equals(btnNederdelPlus)) {
            nederdelAmount.setText("" + (Integer.parseInt(nederdelAmount.getText()) + 1));
        } else if (button.equals(btnJakkePlus)) {
            jakkeAmount.setText("" + (Integer.parseInt(jakkeAmount.getText()) + 1));
        } else if (button.equals(btnBenklaederPlus)) {
            benklaederAmount.setText("" + (Integer.parseInt(benklaederAmount.getText()) + 1));
        } else if (button.equals(btnTroejePlus)) {
            troejeAmount.setText("" + (Integer.parseInt(troejeAmount.getText()) + 1));
        } else if (button.equals(btnTaeppePlus)) {
            taeppeAmount.setText("" + (Integer.parseInt(taeppeAmount.getText()) + 1));
        } else if (button.equals(btnGardinerPlus)) {
            gardinerAmount.setText("" + (Integer.parseInt(gardinerAmount.getText()) + 1));
        }

    }

    /**
     * Set the amount Text when the amount is decremented.
     * @param e
     */
    public void subtractAmount(ActionEvent e) {
        JFXButton button = (JFXButton) e.getSource();
        if (button.equals(btnHabitMinus)) {
            habitAmount.setText("" + (Integer.parseInt(habitAmount.getText()) - 1));
        } else if (button.equals(btnFrakkeMinus)) {
            frakkeAmount.setText("" + (Integer.parseInt(frakkeAmount.getText()) - 1));
        } else if (button.equals(btnKjoleMinus)) {
            kjoleAmount.setText("" + (Integer.parseInt(kjoleAmount.getText()) - 1));
        } else if (button.equals(btnDragtMinus)) {
            dragtAmount.setText("" + (Integer.parseInt(dragtAmount.getText()) - 1));
        } else if (button.equals(btnCottonCoatMinus)) {
            cottonCoatAmount.setText("" + (Integer.parseInt(cottonCoatAmount.getText()) - 1));
        } else if (button.equals(btnVindjakkeMinus)) {
            vindjakkeAmount.setText("" + (Integer.parseInt(vindjakkeAmount.getText()) - 1));
        } else if (button.equals(btnBluseMinus)) {
            bluseAmount.setText("" + (Integer.parseInt(bluseAmount.getText()) - 1));
        } else if (button.equals(btnNederdelMinus)) {
            nederdelAmount.setText("" + (Integer.parseInt(nederdelAmount.getText()) - 1));
        } else if (button.equals(btnJakkeMinus)) {
            jakkeAmount.setText("" + (Integer.parseInt(jakkeAmount.getText()) - 1));
        } else if (button.equals(btnBenklaederMinus)) {
            benklaederAmount.setText("" + (Integer.parseInt(benklaederAmount.getText()) - 1));
        } else if (button.equals(btnTroejeMinus)) {
            troejeAmount.setText("" + (Integer.parseInt(troejeAmount.getText()) - 1));
        } else if (button.equals(btnTaeppeMinus)) {
            taeppeAmount.setText("" + (Integer.parseInt(taeppeAmount.getText()) - 1));
        } else if (button.equals(btnGardinerMinus)) {
            gardinerAmount.setText("" + (Integer.parseInt(gardinerAmount.getText()) - 1));
        }
    }

    /**
     * Add the selected category to the arraylist.
     * @param e
     * @throws SQLException
     */
    public void handleSelectCategory(ActionEvent e) throws SQLException {
        ToggleButton selectedButton = (ToggleButton) e.getSource();
        String categoryName = selectedButton.getText();
        category = new Category();

        if (selectedButton.isSelected() && selectedButton.equals(btnHabit)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(0, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnFrakke)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(1, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnKjole)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(2, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnDragt)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(3, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnCottonCoat)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(4, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnVindjakke)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(5, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnBluse)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(6, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnNederdel)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(7, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnJakke)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(8, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnBenklaeder)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(9, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnTroeje)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(10, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnTaeppe)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(11, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else if (selectedButton.isSelected() && selectedButton.equals(btnGardiner)) {
            selectedButton.setStyle("-fx-background-color: #8cff00; -fx-text-fill: BLACK;");
            categoryList.add(12, category.getCategoryId(selectedButton.getText()));
            System.out.println("added: " + categoryName);

        } else {
            categoryList.remove(categoryList.indexOf(category.getCategoryId(selectedButton.getText())));
            System.out.println("removed " + categoryName);
            selectedButton.setStyle("-fx-background-color: #FF8C00; -fx-text-fill: BLACK;");
        }
    }

    /**
     * Get all the amounts, and add them the the amountList.
     */
    public void getAmount() {
        for (int i = 0; i < amountTextList.size(); i++) {
            if (Integer.parseInt(amountTextList.get(i).getText()) > 0) {
                amountList.add(i, Integer.parseInt(amountTextList.get(i).getText()));
            }
        }
    }

    /**
     * Navigate to the previous menu.
     */
    public void backButton() {
        Animations.fadeOut(SceneNavigator.SHOP_PRELIMINARY, root);
    }

    /**
     * Create a new order.
     * @throws SQLException
     * @throws IOException
     * @throws WriterException
     */
    public void createOrder() throws SQLException, IOException, WriterException {
        try {
            long customerId = HandleID.getUniqueId();
            String name = txfName.getText();
            String email = txfEmail.getText();
            String phone = txfPhone.getText();

            //input validation
            if (name.equals("") || email.equals("") || phone.equals("")) {
                JFXDialogLayout content = new JFXDialogLayout();
                content.setStyle("-fx-background-color: #272727;");
                Text headline = new Text("Advarsel");
                headline.setStyle("-fx-fill: #FF8C00;");
                content.setHeading(headline);

                Text bodyText = new Text("Venligst udfyld alle felter ");
                bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
                content.setBody(bodyText);

                JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
                JFXButton closeButton = new JFXButton("Luk");
                closeButton.setStyle("-fx-fill: #000000");
                closeButton.setStyle("-fx-background-color: #FF8C00");

                closeButton.setOnAction(event -> {
                    dialog.close();
                });
                content.setActions(closeButton);
                dialog.show();
            } else if (!email.contains("@")) {
                JFXDialogLayout content = new JFXDialogLayout();
                content.setStyle("-fx-background-color: #272727;");
                Text headline = new Text("Advarsel");
                headline.setStyle("-fx-fill: #FF8C00;");
                content.setHeading(headline);

                Text bodyText = new Text("Venligst indtast en gyldig email");
                bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
                content.setBody(bodyText);

                JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
                JFXButton closeButton = new JFXButton("Luk");
                closeButton.setStyle("-fx-fill: #000000");
                closeButton.setStyle("-fx-background-color: #FF8C00");

                closeButton.setOnAction(event -> {
                    dialog.close();
                });
                content.setActions(closeButton);
                dialog.show();
            } else {
                //create new customer
                Customer.createCustomer(customerId, name, email, phone);

                long orderId = HandleID.getUniqueId();
                String readableId = HandleID.getUniqueHumanReadableID();
                long deliverPointId = LoginPromptController.user.getDeliveryPointId();
                long centralId = LoginPromptController.user.getCentralId();

                //Create the order.
                getAmount();
                Order.createOrder(orderId, deliverPointId, customerId, centralId, readableId);
                OrderContent.createContent(orderId, categoryList, amountList);
                QRCode.generateOrderQRCode("" + orderId, 500, 500, "src/QRCodes/" + orderId + ".png");

                //Send an order confirmation to the customer's mail.
                Thread thread = new Thread(() -> {
                    Mailer.send(email, "ORDREBEKRÆFTELSE - Dry Cleaning Service", "Kære " + name +
                            "\n\n Tak fordi du har benyttet Dry Cleaning Service. " +
                            "\n Du vil modtage en besked, når dit tøj er klar til afhentning. " +
                            "\n Kundenummer: " + customerId +
                            "\n Ordrenummer: " + orderId +
                            "\n\n FREMVIS DETTE ID VED AFHENTNING: " + readableId, "src/QRCodes/" + orderId + ".png");
                });
                thread.start();

                //Create information dialog
                JFXDialogLayout content = new JFXDialogLayout();
                content.setStyle("-fx-background-color: #272727;");
                Text headline = new Text("Ordre oprettet");
                headline.setStyle("-fx-fill: #FF8C00;");
                content.setHeading(headline);

                Text bodyText = new Text("Ordren er blevet oprettet");
                bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
                content.setBody(bodyText);

                JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
                JFXButton closeButton = new JFXButton("Luk");
                closeButton.setStyle("-fx-fill: #000000");
                closeButton.setStyle("-fx-background-color: #FF8C00");

                closeButton.setOnAction(event -> {

                    Animations.fadeOut(SceneNavigator.SHOP_PRELIMINARY, root);

                    dialog.close();
                });

                content.setActions(closeButton);
                dialog.show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}