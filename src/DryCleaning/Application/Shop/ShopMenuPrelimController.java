package DryCleaning.Application.Shop;

import DryCleaning.Functionality.Animations;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.*;
import com.jfoenix.controls.JFXButton;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Duration;

import static javafx.animation.Interpolator.EASE_BOTH;

public class ShopMenuPrelimController {

    public static String buttonClicked;
    @FXML
    private BorderPane borderPane;

    @FXML
    private JFXButton btnCreateOrder, btnGetOrder, backButtonPrelim;

    @FXML
    private Text orderText, pickupText;
    private boolean hasSelected = false;

    public void initialize() {
        btnCreateOrder.setOpacity(0);
        btnGetOrder.setOpacity(0);
        orderText.setOpacity(0);
        pickupText.setOpacity(0);
        backButtonPrelim.setOpacity(0);

        btnCreateOrder.setOnMouseExited(t -> handleHoverAnimationShop(btnCreateOrder, 1));
        btnCreateOrder.setOnMouseEntered(t -> handleHoverAnimationShop(btnCreateOrder, 0));
        btnCreateOrder.setOnMousePressed(t -> {
            buttonClicked = "deliverypoint";
            handleClickAnimationShop(btnCreateOrder, 0);
        });

        btnGetOrder.setOnMouseExited(t -> handleHoverAnimationShop(btnGetOrder, 1));
        btnGetOrder.setOnMouseEntered(t -> handleHoverAnimationShop(btnGetOrder, 0));
        btnGetOrder.setOnMousePressed(t -> {
            buttonClicked = "driver";
            handleClickAnimationShop(btnGetOrder, 1);
        });

        Timeline fadeGUI = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(backButtonPrelim.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(pickupText.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(orderText.opacityProperty(),
                        1,
                        EASE_BOTH)));

        Thread animationThread = new Thread(() -> {
            try {
                Thread.sleep(300);
                assignAnimationShop(btnCreateOrder);
                Thread.sleep(200);
                assignAnimationShop(btnGetOrder);
                Thread.sleep(500);
                fadeGUI.play();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        animationThread.start();
    }

    /**
     * Navigate to the main menu.
     */
    public void backButtonShopprelim() {
        Animations.fadeOut(SceneNavigator.MainMenu, borderPane);

    }

    /**
     * Handle button clicks
     * @param button
     * @param clickedButton
     */
    private void handleClickAnimationShop(JFXButton button, int clickedButton) {
        hasSelected = true;

        Timeline buttonClick = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(button.scaleXProperty(),
                        1.75,
                        EASE_BOTH),
                new KeyValue(button.scaleYProperty(),
                        1.75,
                        EASE_BOTH)));

        switch (clickedButton) {
            case 0: // btnCreateOrder
                buttonClick.play();
                new FadeOut(btnGetOrder).play();
                new FadeOut(btnCreateOrder).play();
                new FadeOut(orderText).play();
                new FadeOut(pickupText).play();
                new FadeOut(backButtonPrelim).play();
                buttonClick.setOnFinished(e -> SceneNavigator.loadScene(SceneNavigator.SHOP_MENU));
                break;
            case 1: // btnGetOrder
                buttonClick.play();
                new FadeOut(btnGetOrder).play();
                new FadeOut(btnCreateOrder).play();
                new FadeOut(orderText).play();
                new FadeOut(pickupText).play();
                new FadeOut(backButtonPrelim).play();
                buttonClick.setOnFinished(e -> SceneNavigator.loadScene(SceneNavigator.SHOP_PICKUP));
                break;
            default:
                break;
        }
    }

    /**
     * Create fade in animation
     * @param button
     */
    private void assignAnimationShop(JFXButton button) {
        new FadeInUp(button).play();
    }

    /**
     * Animation when hovered over
     * @param button
     * @param buttonStatus
     */
    private void handleHoverAnimationShop(JFXButton button, int buttonStatus) {
        if (hasSelected == true) {
            // Nothing!
        } else {
            Timeline buttonHover = new Timeline(new KeyFrame(Duration.millis(75),
                    new KeyValue(button.scaleXProperty(),
                            1.1,
                            EASE_BOTH),
                    new KeyValue(button.scaleYProperty(),
                            1.1,
                            EASE_BOTH)));

            Timeline buttonUnhover = new Timeline(new KeyFrame(Duration.millis(75),
                    new KeyValue(button.scaleXProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(button.scaleYProperty(),
                            1,
                            EASE_BOTH)));

            if (buttonStatus == 0) {
                buttonHover.play();
            } else if (buttonStatus == 1) {
                buttonUnhover.play();
            }
        }
    }
}