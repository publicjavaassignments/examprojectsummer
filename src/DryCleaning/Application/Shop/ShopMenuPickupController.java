package DryCleaning.Application.Shop;

import DryCleaning.Functionality.Order;
import DryCleaning.Functionality.QRCode;
import DryCleaning.SceneNavigator;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.File;
import java.sql.SQLException;

import static javafx.animation.Interpolator.EASE_BOTH;

public class ShopMenuPickupController {
    private String qrText = "";

    @FXML
    private HBox dragboardBox;
    @FXML
    private VBox qrEffect;
    @FXML
    private BorderPane fadeInBP;
    @FXML
    private ImageView imageViewDelivery, orderScanEffect, imageViewDelivery2;
    @FXML
    private StackPane alertStage;

    public void initialize() {
        dragboardBox.setOnMouseExited(t -> handleScanEffect(qrEffect, 1));
        dragboardBox.setOnMouseEntered(t -> handleScanEffect(qrEffect, 0));

        handleDragBoard();
        fadeInBP.setOpacity(0);
        orderScanEffect.setOpacity(0);
        imageViewDelivery.setOpacity(0);
        imageViewDelivery2.setOpacity(0);
        Timeline fadeGUI = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(fadeInBP.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(imageViewDelivery.opacityProperty(),
                        1,
                        EASE_BOTH)));

        Thread animationThread = new Thread(() -> {
            try {
                Thread.sleep(300);
                fadeGUI.play();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        animationThread.start();
    }

    /**
     * Create animation when order is scanned.
     * @param dragBoard
     * @param buttonStatus
     */
    private void handleScanEffect(VBox dragBoard, int buttonStatus) {
        // orderScanEffect - Imageview
        Timeline dragboardHover = new Timeline(new KeyFrame(Duration.millis(75),
                new KeyValue(dragBoard.scaleXProperty(),
                        1.1,
                        EASE_BOTH),
                new KeyValue(orderScanEffect.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(imageViewDelivery2.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(imageViewDelivery.opacityProperty(),
                        0,
                        EASE_BOTH),
                new KeyValue(orderScanEffect.scaleXProperty(),
                        0.925,
                        EASE_BOTH),
                new KeyValue(orderScanEffect.scaleYProperty(),
                        0.925,
                        EASE_BOTH),
                new KeyValue(dragBoard.scaleYProperty(),
                        1.1,
                        EASE_BOTH)));

        Timeline dragboardUnhover = new Timeline(new KeyFrame(Duration.millis(75),
                new KeyValue(dragBoard.scaleXProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(orderScanEffect.scaleXProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(orderScanEffect.scaleYProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(imageViewDelivery2.opacityProperty(),
                        0,
                        EASE_BOTH),
                new KeyValue(imageViewDelivery.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(orderScanEffect.opacityProperty(),
                        0,
                        EASE_BOTH),
                new KeyValue(dragBoard.scaleYProperty(),
                        1,
                        EASE_BOTH)));

        if (buttonStatus == 0) {
            dragboardHover.play();
        } else if (buttonStatus == 1) {
            dragboardUnhover.play();
        }
    }

    /**
     * Create a dialog when QRCode is scanned.
     * @param OrderID
     * @throws SQLException
     */
    private void scanDialog(long OrderID) throws SQLException {
        Order order = new Order();
        order.getDetails(OrderID);

        long orderID = order.getOrderId();
        String orderIDTreated = String.valueOf(orderID);

        String humanReadableID = order.getReadableId();

        JFXDialogLayout content = new JFXDialogLayout();
        Text headline = new Text("Ordre ID: " + orderIDTreated);
        headline.setStyle("-fx-fill: #FF8C00; -fx-font: 32 roboto;");
        content.setHeading(headline);
        Text packageID = new Text("Pakke ID: " + humanReadableID);
        packageID.setStyle("-fx-fill: #FF8C00; -fx-font: 24 roboto;");
        VBox vbox = new VBox();
        Text deliveryNote = new Text("\nNår kunden har modtaget pakken, tryk \'Udleveret\' for at lukke ordren.");
        deliveryNote.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto;");
        ImageView qrCode = QRCode.generateQRCodeImageView(orderIDTreated);

        vbox.getChildren().addAll(qrCode, packageID, deliveryNote);
        vbox.setAlignment(Pos.CENTER);
        content.setBody(vbox);
        content.setStyle("-fx-background-color: #272727;");

        JFXDialog dialog = new JFXDialog(alertStage, content, JFXDialog.DialogTransition.CENTER);
        JFXButton deliverButton = new JFXButton("Udleveret");
        deliverButton.setStyle("-fx-fill: #FF8C00");
        deliverButton.setStyle("-fx-background-color: #FF8C00");
        JFXButton cancelButton = new JFXButton("Afbryd");
        cancelButton.setStyle("-fx-fill: #FF8C00");
        cancelButton.setStyle("-fx-background-color: #FF8C00");

        deliverButton.setOnAction(event -> {
            try {
                Order.setStatus(Long.parseLong(qrText));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            fadeBGIn();
            dialog.close();
            goBackButtonPickup();
        });
        cancelButton.setOnAction(event -> {
            fadeBGIn();
            dialog.close();
        });
        content.setActions(deliverButton, cancelButton);
        fadeBGOut();
        dialog.show();
    }

    /**
     * Fade in background
     */
    public void fadeBGIn() {
        Timeline fadeBGIn = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(qrEffect.opacityProperty(),
                        1,
                        EASE_BOTH)));
        fadeBGIn.play();
    }

    /**
     * Fade out background
     */
    public void fadeBGOut() {
        Timeline fadeBGOut = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(qrEffect.opacityProperty(),
                        0,
                        EASE_BOTH)));
        fadeBGOut.play();
    }

    /**
     * Navigate to previous menu.
     */
    public void goBackButtonPickup() {
        Timeline buttonClick = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(fadeInBP.scaleXProperty(),
                        0.25,
                        EASE_BOTH),
                new KeyValue(fadeInBP.opacityProperty(),
                        0,
                        EASE_BOTH),
                new KeyValue(imageViewDelivery.scaleXProperty(),
                        0.25,
                        EASE_BOTH),
                new KeyValue(imageViewDelivery.scaleYProperty(),
                        0,
                        EASE_BOTH),
                new KeyValue(imageViewDelivery.opacityProperty(),
                        0,
                        EASE_BOTH),
                new KeyValue(fadeInBP.scaleYProperty(),
                        0.25,
                        EASE_BOTH)));
        buttonClick.play();
        buttonClick.setOnFinished(e -> SceneNavigator.loadScene(SceneNavigator.SHOP_PRELIMINARY));
    }

    /**
     * Create dragboard
     */
    public void handleDragBoard() {
        try {
            dragboardBox.setOnDragOver((dragEvent) -> {
                Dragboard db = dragEvent.getDragboard();
                if (db.hasFiles()) {
                    dragEvent.acceptTransferModes(TransferMode.COPY);
                } else {
                    dragEvent.consume();
                }
            });

            dragboardBox.setOnDragDropped((dragEvent) -> {
                Dragboard db = dragEvent.getDragboard();
                if (db.hasFiles()) {
                    for (File file : db.getFiles()) {
                        try {
                            qrText = QRCode.read(file);
                            scanDialog(Long.parseLong(qrText));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}