package DryCleaning.Application.Main;

import DryCleaning.Application.Driver.DriverMenuPrelimController;
import DryCleaning.Functionality.Transport;
import DryCleaning.Functionality.User;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.FadeInUp;
import Libs.animatefx.animation.FadeOut;
import Libs.animatefx.animation.FadeOutLeft;
import Libs.animatefx.animation.FadeOutRight;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.sql.SQLException;

import static javafx.animation.Interpolator.EASE_BOTH;

public class MainMenuController {

    public static String buttonClicked;

    @FXML
    private StackPane root;
    @FXML

    private JFXButton btnShop, btnTruck, btnFactory, btnAdmin;
    private boolean hasSelected = false;
    public static boolean permission = true;

    public void initialize() throws SQLException {

        btnShop.setOpacity(0);
        btnTruck.setOpacity(0);
        btnFactory.setOpacity(0);

        btnShop.setOnMouseExited(t -> handleHoverAnimation(btnShop, 1));
        btnShop.setOnMouseEntered(t -> handleHoverAnimation(btnShop, 0));
        btnShop.setOnMousePressed(t -> {
            buttonClicked = "deliverypoint";
            handleClickAnimation(btnShop, 0);
        });

        btnTruck.setOnMouseExited(t -> handleHoverAnimation(btnTruck, 1));
        btnTruck.setOnMouseEntered(t -> handleHoverAnimation(btnTruck, 0));
        btnTruck.setOnMousePressed(t -> {
            buttonClicked = "driver";
            handleClickAnimation(btnTruck, 1);
        });

        btnFactory.setOnMouseExited(t -> handleHoverAnimation(btnFactory, 1));
        btnFactory.setOnMouseEntered(t -> handleHoverAnimation(btnFactory, 0));
        btnFactory.setOnMousePressed(t -> {
            buttonClicked = "central";
            handleClickAnimation(btnFactory, 2);
        });

        btnAdmin.setOnMouseExited(t -> handleHoverAnimation(btnAdmin, 1));
        btnAdmin.setOnMouseEntered(t -> handleHoverAnimation(btnAdmin, 0));
        btnAdmin.setOnMousePressed(t -> {
            buttonClicked = "admin";
            handleClickAnimation(btnAdmin, 3);
        });


        Thread animationThread = new Thread(() -> {
            try {
                Thread.sleep(300);
                assignAnimation(btnShop);
                Thread.sleep(200);
                assignAnimation(btnTruck);
                Thread.sleep(200);
                assignAnimation(btnFactory);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        animationThread.start();

        //Show dialog if the user doesnt have permission
        if (permission == false) {
            JFXDialogLayout content = new JFXDialogLayout();
            content.setStyle("-fx-background-color: #272727;");
            Text headline = new Text("Advarsel");
            headline.setStyle("-fx-fill: #FF8C00;");
            content.setHeading(headline);

            Text bodyText = new Text("Du har ikke adgang til denne side.");
            bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
            content.setBody(bodyText);

            JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
            JFXButton closeButton = new JFXButton("Luk");
            closeButton.setStyle("-fx-fill: #000000");
            closeButton.setStyle("-fx-background-color: #FF8C00");

            closeButton.setOnAction(event -> {
                dialog.close();
            });

            content.setActions(closeButton);
            dialog.show();
        }
    }

    /**
     * Handle button clicks.
     *
     * @param button
     * @param clickedButton
     */
    private void handleClickAnimation(JFXButton button, int clickedButton) {
        hasSelected = true;

        Timeline buttonClick = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(button.scaleXProperty(),
                        1.75,
                        EASE_BOTH),
                new KeyValue(button.scaleYProperty(),
                        1.75,
                        EASE_BOTH)));

        switch (clickedButton) {
            case 0: // Shop
                buttonClick.play();
                new FadeOut(btnShop).play();
                new FadeOutRight(btnTruck).play();
                new FadeOutRight(btnFactory).play();

                buttonClick.setOnFinished(e -> {
                    if (LoginPromptController.user.getPermDeliveryPoint()) {
                        SceneNavigator.loadScene(SceneNavigator.SHOP_PRELIMINARY);
                    } else {
                        SceneNavigator.loadScene(SceneNavigator.LoginPrompt);
                    }
                });
                break;
            case 1: // Truck
                buttonClick.play();
                new FadeOut(btnTruck).play();
                new FadeOutRight(btnFactory).play();
                new FadeOutLeft(btnShop).play();

                buttonClick.setOnFinished(e -> {
                    if (LoginPromptController.user.getPermDriver()) {
                        Transport transport = new Transport();

                        boolean isDone = true;
                        try {
                            isDone = transport.getNewestTransportStatus(LoginPromptController.user.getDriverId());
                            DriverMenuPrelimController.transportId = transport.getTransportId();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }

                        //If the last route is not finished, resume that route, else navigate to the driver preliminary.
                        if (isDone == false) {
                            SceneNavigator.loadScene(SceneNavigator.DRIVER_MENU);
                        } else {
                            SceneNavigator.loadScene(SceneNavigator.DRIVER_PRELIMINARY);
                        }
                    } else {
                        SceneNavigator.loadScene(SceneNavigator.LoginPrompt);
                    }
                });
                break;
            case 2: // Factory
                buttonClick.play();
                new FadeOut(btnFactory).play();
                new FadeOutLeft(btnTruck).play();
                new FadeOutLeft(btnShop).play();

                buttonClick.setOnFinished(e -> {
                    if (LoginPromptController.user.getPermCentral()) {
                        SceneNavigator.loadScene(SceneNavigator.CENTRAL_PRELIMINARY);
                    } else {
                        SceneNavigator.loadScene(SceneNavigator.LoginPrompt);
                    }
                });
                break;
            case 3: //Admin
                buttonClick.play();
                new FadeOut(btnFactory).play();
                new FadeOutLeft(btnTruck).play();
                new FadeOutLeft(btnShop).play();

                buttonClick.setOnFinished(e -> {
                    if (LoginPromptController.user.getPermAdmin()) {
                        SceneNavigator.loadScene(SceneNavigator.ADMIN_PANEL_STATISTICS);
                    } else {
                        SceneNavigator.loadScene(SceneNavigator.LoginPrompt);
                    }
                });
                break;
            default:
                break;
        }
    }

    /**
     * Create fadeinup animation
     * @param button
     */
    private void assignAnimation(JFXButton button) {
        new FadeInUp(button).play();
    }

    /**
     * Animation when hovered over
     * @param button
     * @param buttonStatus
     */
    private void handleHoverAnimation(JFXButton button, int buttonStatus) {
        if (hasSelected == true) {
            // Nothing!
        } else {
            Timeline buttonHover = new Timeline(new KeyFrame(Duration.millis(75),
                    new KeyValue(button.scaleXProperty(),
                            1.1,
                            EASE_BOTH),
                    new KeyValue(button.scaleYProperty(),
                            1.1,
                            EASE_BOTH)));

            Timeline buttonUnhover = new Timeline(new KeyFrame(Duration.millis(75),
                    new KeyValue(button.scaleXProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(button.scaleYProperty(),
                            1,
                            EASE_BOTH)));

            if (buttonStatus == 0) {
                buttonHover.play();
            } else if (buttonStatus == 1) {
                buttonUnhover.play();
            }
        }
    }

    /**
     * Logout user
     */
    public void handleLogOut() {
        LoginPromptController.loggedIn = false;
        LoginPromptController.user.reset();
        LoginPromptController.user = User.getInstance();

        JFXDialogLayout content = new JFXDialogLayout();
        content.setStyle("-fx-background-color: #272727;");

        Text headingText = new Text("Information");
        headingText.setStyle("-fx-fill: #FF8C00;");

        content.setHeading(headingText);
        Text bodeText = new Text("Du er nu logget ud");
        bodeText.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto;");

        content.setBody(bodeText);

        JFXButton closeButton = new JFXButton("Luk");
        closeButton.setStyle("-fx-background-color: #FF8C00; -fx-fill: #FF8C00");

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        closeButton.setOnAction(event -> dialog.close());
        content.setActions(closeButton);
        dialog.show();
    }
}