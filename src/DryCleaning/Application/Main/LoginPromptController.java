package DryCleaning.Application.Main;

import DryCleaning.Application.Driver.DriverMenuPrelimController;
import DryCleaning.Database.SQLServer;
import DryCleaning.Functionality.Animations;
import DryCleaning.Functionality.Transport;
import DryCleaning.Functionality.User;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.AnimateFXInterpolator;
import Libs.animatefx.animation.FadeInDown;
import Libs.animatefx.animation.Shake;
import com.jfoenix.controls.*;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import static javafx.animation.Interpolator.EASE_BOTH;

public class LoginPromptController {
    public static User user = User.getInstance();
    @FXML
    private StackPane root;
    @FXML
    private VBox LoginPromptAnimate;
    @FXML
    private Text warningText;
    @FXML
    private JFXPasswordField passwordPrompt;
    @FXML
    private JFXTextField usernamePrompt;
    @FXML
    private JFXSpinner spinner;
    public static boolean loggedIn;

    public void initialize() {
        spinner.setOpacity(0);
        LoginPromptAnimate.setOpacity(0);
        Thread animationThread = new Thread(() -> {
            try {
                Thread.sleep(100);
                new FadeInDown(LoginPromptAnimate).play();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        animationThread.start();
    }

    /**
     * Navigate to the main menu.
     */
    public void backHandler() {
        Animations.fadeOut(SceneNavigator.MainMenu, root);

    }

    /**
     * Reset password textfield
     */
    public void resetPassword() {
        passwordPrompt.setUnFocusColor(Paint.valueOf("#ff8c00"));
    }

    /**
     * Reset username textfield
     */
    public void resetUsername() {
        usernamePrompt.setUnFocusColor(Paint.valueOf("#ff8c00"));
    }

    /**
     * Handle login functionality
     */
    public void entryHandler() {
        // Error text timeline animations for fading
        Timeline fadeIn = new Timeline(
                new KeyFrame((Duration.millis(0)),
                        new KeyValue(warningText.opacityProperty(), 0, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleXProperty(), 0.3, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleYProperty(), 0.3, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleZProperty(), 0.3, AnimateFXInterpolator.EASE)),
                new KeyFrame(Duration.millis(125),
                        new KeyValue(warningText.opacityProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleXProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleYProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleZProperty(), 1, AnimateFXInterpolator.EASE)),
                new KeyFrame(Duration.millis(250),
                        new KeyValue(warningText.opacityProperty(), 1, AnimateFXInterpolator.EASE)));

        Timeline fadeOut = new Timeline(
                new KeyFrame((Duration.millis(0)),
                        new KeyValue(warningText.opacityProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleXProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleYProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleZProperty(), 0.3, AnimateFXInterpolator.EASE)),
                new KeyFrame(Duration.millis(125),
                        new KeyValue(warningText.opacityProperty(), 0, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleXProperty(), 0.3, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleYProperty(), 0.3, AnimateFXInterpolator.EASE),
                        new KeyValue(warningText.scaleZProperty(), 0.3, AnimateFXInterpolator.EASE)),
                new KeyFrame(Duration.millis(250),
                        new KeyValue(warningText.opacityProperty(), 0, AnimateFXInterpolator.EASE)));

        Timeline fadeOutUp = new Timeline(
                new KeyFrame(Duration.millis(0),
                        new KeyValue(LoginPromptAnimate.opacityProperty(), 1, AnimateFXInterpolator.EASE),
                        new KeyValue(LoginPromptAnimate.translateYProperty(), 0, AnimateFXInterpolator.EASE)),
                new KeyFrame(Duration.millis(1000),
                        new KeyValue(LoginPromptAnimate.opacityProperty(), 0, AnimateFXInterpolator.EASE),
                        new KeyValue(LoginPromptAnimate.translateYProperty(), -LoginPromptAnimate.getBoundsInParent().getHeight(), AnimateFXInterpolator.EASE)));

        int promptStatus = 0;

        /*
        Rather than putting the different sub-events into a large if-statement, we found it easier to have an if
        that simply sets an integer, and then have a switch case which handles the actual functionality.
        This makes it a lot easier to get an overview of what's going on.
        */

        if (usernamePrompt.getText().equals("") && passwordPrompt.getText().equals("")) {
            promptStatus = 0;
        } else if (passwordPrompt.getText().equals("")) {
            promptStatus = 1;
        } else if (usernamePrompt.getText().equals("")) {
            promptStatus = 2;
        } else {
            promptStatus = 3;
        }


        switch (promptStatus) {
            case 0: // Missing password & username
                passwordPrompt.setUnFocusColor(Paint.valueOf("#ff0000"));
                usernamePrompt.setUnFocusColor(Paint.valueOf("#ff0000"));
                new Shake(usernamePrompt).play();
                new Shake(passwordPrompt).play();
                if (warningText.getText().equals(" ")) { // If the warning text is empty
                    warningText.setText("Et brugernavn og en adgangskode er påkrævet.");
                    fadeIn.play();
                } else if (warningText.getText().equals("Et brugernavn og en adgangskode er påkrævet.")) { // Already there
                    new Shake(warningText).play();
                } else { // If there is already text, but it's not the same error
                    fadeOut.play();
                    fadeOut.setOnFinished(event -> {
                        warningText.setText("Et brugernavn og en adgangskode er påkrævet.");
                        fadeIn.play();
                    });
                }
                break;
            case 1: // Missing password
                passwordPrompt.setUnFocusColor(Paint.valueOf("#ff0000"));
                usernamePrompt.setUnFocusColor(Paint.valueOf("#ff8c00"));
                new Shake(passwordPrompt).play();
                if (warningText.getText().equals(" ")) { // If the warning text is empty
                    warningText.setText("En adgangskode er påkrævet.");
                    fadeIn.play();
                } else if (warningText.getText().equals("En adgangskode er påkrævet.")) { // Already there
                    new Shake(warningText).play();
                } else { // If there is already text, but it's not the same error
                    fadeOut.play();
                    fadeOut.setOnFinished(event -> {
                        warningText.setText("En adgangskode er påkrævet.");
                        fadeIn.play();
                    });
                }
                break;
            case 2: // Missing username
                passwordPrompt.setUnFocusColor(Paint.valueOf("#ff8c00"));
                usernamePrompt.setUnFocusColor(Paint.valueOf("#ff0000"));
                new Shake(usernamePrompt).play();
                if (warningText.getText().equals(" ")) { // If the warning text is empty
                    warningText.setText("Et brugernavn er påkrævet.");
                    fadeIn.play();
                } else if (warningText.getText().equals("Et brugernavn er påkrævet.")) { // Already there
                    new Shake(warningText).play();
                } else { // If there is already text, but it's not the same error
                    fadeOut.play();
                    fadeOut.setOnFinished(event -> {
                        warningText.setText("Et brugernavn er påkrævet.");
                        fadeIn.play();
                    });
                }
                break;
            case 3: // Check password!
                passwordPrompt.setUnFocusColor(Paint.valueOf("#ff8c00"));
                usernamePrompt.setUnFocusColor(Paint.valueOf("#ff8c00"));
                fadeOut.play();

                Timeline loaderFadeOut = new Timeline(new KeyFrame(Duration.millis(250),
                        new KeyValue(spinner.scaleXProperty(),
                                0,
                                EASE_BOTH),
                        new KeyValue(spinner.scaleYProperty(),
                                0,
                                EASE_BOTH),
                        new KeyValue(spinner.opacityProperty(),
                                0,
                                EASE_BOTH)));

                Timeline loaderFadeIn = new Timeline(new KeyFrame(Duration.millis(250),
                        new KeyValue(spinner.scaleXProperty(),
                                1,
                                EASE_BOTH),
                        new KeyValue(spinner.scaleYProperty(),
                                1,
                                EASE_BOTH),
                        new KeyValue(spinner.opacityProperty(),
                                1,
                                EASE_BOTH)));
                loaderFadeIn.play();

                Thread thread = new Thread(() -> {
                    try {
                        SQLServer.connect();
                        String username = usernamePrompt.getText();

                        //Hash password
                        String password = User.hashPassword(passwordPrompt.getText());

                        //Login user.
                        loggedIn = user.login(username, password);

                        //Check if user has permission to access the menu.
                        if (MainMenuController.buttonClicked.equals("deliverypoint") && loggedIn == true) {
                            if (user.getPermDeliveryPoint() == true) {
                                fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.SHOP_PRELIMINARY));
                            } else {
                                MainMenuController.permission = false;
                                fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.MainMenu));
                            }

                        } else if (MainMenuController.buttonClicked.equals("driver") && loggedIn == true) {

                            if (user.getPermDriver() == true) {
                                Transport transport = new Transport();
                                boolean isDone = transport.getNewestTransportStatus(LoginPromptController.user.getDriverId());

                                //If the last route is not finished, resume that route, else navigate to the driver preliminary.
                                if (isDone == false) {
                                    fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.DRIVER_MENU));
                                    DriverMenuPrelimController.transportId = transport.getTransportId();
                                } else {
                                    MainMenuController.permission = false;
                                    fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.DRIVER_PRELIMINARY));
                                }
                            } else {
                                fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.MainMenu));
                            }


                        } else if (MainMenuController.buttonClicked.equals("central") && loggedIn == true) {

                            if (user.getPermCentral() == true) {
                                fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.CENTRAL_PRELIMINARY));
                            } else {
                                MainMenuController.permission = false;
                                fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.MainMenu));
                            }
                        } else if (MainMenuController.buttonClicked.equals("admin") && loggedIn == true) {
                            if (user.getPermAdmin() == true) {
                                System.out.println("test");
                                fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.ADMIN_PANEL_STATISTICS));
                            } else {
                                System.out.println("test2");
                                MainMenuController.permission = false;
                                fadeOutUp.setOnFinished(event -> SceneNavigator.loadScene(SceneNavigator.MainMenu));
                            }
                        } else if (loggedIn == false) {
                            fadeOut.play();
                            fadeOut.setOnFinished(event -> {
                                warningText.setText("Brugernavn eller adgangskode er forkert");
                                new Shake(warningText).play();
                                fadeIn.play();
                            });
                            loaderFadeOut.play();
                        }

                    } catch (NoSuchAlgorithmException | SQLException e) {
                        e.printStackTrace();
                    } finally {
                        if (loggedIn) {
                            loaderFadeOut.play();
                            try {
                                Thread.sleep(200);
                                fadeOutUp.play();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                thread.start();
                break;
            default:
                break;
        }
    }
}