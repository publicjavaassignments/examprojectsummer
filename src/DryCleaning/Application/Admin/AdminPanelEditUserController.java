package DryCleaning.Application.Admin;

import DryCleaning.Functionality.*;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.ZoomIn;
import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;

public class AdminPanelEditUserController {

    @FXML
    private StackPane root;
    @FXML
    private JFXTextField txfUsername, txfPassword, txfSearch;
    @FXML
    private JFXComboBox comboCentral, comboDriver, comboDelivery;
    @FXML
    private JFXCheckBox checkBoxCentral, checkBoxDriver, checkBoxDelivery, checkBoxAdmin;

    private ArrayList<Central> centralList = new ArrayList<>();
    private ArrayList<Driver> driverList = new ArrayList<>();
    private ArrayList<DeliveryPoint> deliveryPointList = new ArrayList<>();

    public void initialize() throws SQLException {
        root.setOpacity(0);

        //Populate combo boxes
        centralList = Central.getCentrals();
        comboCentral.getItems().add(0);
        for (Central central : centralList) {
            comboCentral.getItems().add(central.getName());
        }

        driverList = Driver.getDrivers();
        comboDriver.getItems().add(0);
        for (Driver driver : driverList) {
            comboDriver.getItems().add(driver.getName());
        }

        deliveryPointList = DeliveryPoint.getDeliveryPoints();
        comboDelivery.getItems().add(0);
        for (DeliveryPoint deliveryPoint : deliveryPointList) {
            comboDelivery.getItems().add(deliveryPoint.getName());
        }
        new ZoomIn(root).play();
    }

    /**
     * Search user information
     */
    public void handleSearch() {
        try {
            String username = txfSearch.getText();
            User user = User.searchUser(username);
            txfUsername.setText(user.getUsername());

            if (user.getCentralId() == 0) {
                comboCentral.setValue("0");
            } else {
                for (Central central : centralList) {
                    if (user.getCentralId() == central.getId()) {
                        comboCentral.setValue(central.getName());
                    }
                }
            }


            if (user.getDriverId() == 0) {
                comboDriver.setValue(0);
                System.out.println(comboDriver.getValue());
            } else {
                for (Driver driver : driverList) {
                    if (user.getDriverId() == driver.getDriverId()) {
                        comboDriver.setValue(driver.getName());
                    }
                }
            }
            if (user.getDeliveryPointId() == 0) {
                comboDelivery.setValue(0);
            } else {
                for (DeliveryPoint deliveryPoint : deliveryPointList) {
                    if (user.getDeliveryPointId() == deliveryPoint.getId()) {
                        comboDelivery.setValue(deliveryPoint.getName());
                    }
                }
            }

            if (user.getPermCentral() == true)
                checkBoxCentral.setSelected(true);
            else
                checkBoxCentral.setSelected(false);

            if (user.getPermDriver() == true)
                checkBoxDriver.setSelected(true);
            else
                checkBoxDriver.setSelected(false);

            if (user.getPermDeliveryPoint() == true)
                checkBoxDelivery.setSelected(true);
            else
                checkBoxDelivery.setSelected(false);

            if (user.getPermAdmin() == true)
                checkBoxAdmin.setSelected(true);
            else
                checkBoxAdmin.setSelected(false);


        } catch (Exception e) {
            JFXDialogLayout content = new JFXDialogLayout();
            content.setStyle("-fx-background-color: #272727;");
            Text headline = new Text("Advarsel");
            headline.setStyle("-fx-fill: #FF8C00;");
            content.setHeading(headline);

            Text bodyText = new Text("Denne bruger findes ikke");
            bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
            content.setBody(bodyText);

            JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
            JFXButton closeButton = new JFXButton("Luk");
            closeButton.setStyle("-fx-fill: #000000");
            closeButton.setStyle("-fx-background-color: #FF8C00");

            closeButton.setOnAction(event -> {
                dialog.close();
            });

            content.setActions(closeButton);
            dialog.show();
        }
    }


    /**
     * Update user information
     * @throws NoSuchAlgorithmException
     */
    public void handleUpdateUser() throws NoSuchAlgorithmException {
        String username = txfUsername.getText();
        String password = User.hashPassword(txfPassword.getText());
        long centralId = 0;
        long deliveryPointId = 0;
        long driverId = 0;

        if (comboCentral.getValue().equals("0")) {
            centralId = 0;
        } else {
            for (Central central : centralList) {
                if (central.getName().equals(comboCentral.getValue())) {
                    centralId = central.getId();
                }
            }
        }

        if (comboDelivery.getValue().equals("0")) {
            deliveryPointId = 0;
        } else {
            for (DeliveryPoint deliveryPoint : deliveryPointList) {
                if (deliveryPoint.getName().equals(comboDelivery.getValue())) {
                    deliveryPointId = deliveryPoint.getId();
                }
            }
        }

        if (comboDriver.getValue().equals("0")) {
            driverId = 0;
        } else {
            for (Driver driver : driverList) {
                if (driver.getName().equals(comboDriver.getValue())) {
                    driverId = driver.getDriverId();
                }
            }
        }

        boolean permCentral = checkBoxCentral.isSelected();
        boolean permDriver = checkBoxDriver.isSelected();
        boolean permDelivery = checkBoxDelivery.isSelected();
        boolean permAdmin = checkBoxAdmin.isSelected();


        JFXDialogLayout content = new JFXDialogLayout();
        content.setStyle("-fx-background-color: #272727;");
        Text headline = new Text("Opdater bruger");
        headline.setStyle("-fx-fill: #FF8C00;");
        content.setHeading(headline);

        Text bodyText = new Text("Er du sikker på, at du vil opdatere denne bruger?");
        bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
        content.setBody(bodyText);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        JFXButton yesButton = new JFXButton("Ja");
        yesButton.setStyle("-fx-fill: #000000");
        yesButton.setStyle("-fx-background-color: #0e5a1c");

        JFXButton noButton = new JFXButton("Nej");
        noButton.setStyle("-fx-fill: #000000");
        noButton.setStyle("-fx-background-color: #c03a2a");

        long finalDeliveryPointId = deliveryPointId;
        long finalCentralId = centralId;
        long finalDriverId = driverId;
        yesButton.setOnAction(event -> {
            try {
                User.updateUser(username, password, finalDeliveryPointId, finalCentralId, finalDriverId, permDelivery, permCentral, permDriver, permAdmin);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            comboCentral.valueProperty().set(null);
            comboDriver.valueProperty().set(null);
            comboDelivery.valueProperty().set(null);

            checkBoxCentral.setSelected(false);
            checkBoxDriver.setSelected(false);
            checkBoxDelivery.setSelected(false);
            checkBoxAdmin.setSelected(false);

            txfUsername.clear();
            txfPassword.clear();
            dialog.close();
        });

        noButton.setOnAction(e -> dialog.close());
        content.setActions(yesButton, noButton);
        dialog.show();
    }

    /**
     * Navigate to statistics page
     */
    public void handleStatistics() {
        Animations.fadeOut(SceneNavigator.ADMIN_PANEL_STATISTICS, root);
    }

    /**
     * Navigate to new user page
     */
    public void handleNewUser() {
        Animations.fadeOut(SceneNavigator.ADMIN_CREATE_USER, root);
    }

    /**
     * Navigate to new department page.
     */
    public void handleNewDepartment() {
        Animations.fadeOut(SceneNavigator.ADMIN_NEW_DEPARTMENT, root);
    }

    /**
     * Navigate to main menu.
     */
    public void handleBack() {
        Animations.fadeOut(SceneNavigator.MainMenu, root);
    }

}
