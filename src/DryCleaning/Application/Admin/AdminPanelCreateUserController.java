package DryCleaning.Application.Admin;

import DryCleaning.Functionality.*;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.ZoomIn;
import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;


import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;

public class AdminPanelCreateUserController {

    @FXML
    private StackPane root;
    @FXML
    private JFXTextField txfUsername, txfPassword;
    @FXML
    private JFXComboBox<String> comboCentral, comboDriver, comboDelivery;
    @FXML
    private JFXCheckBox checkBoxCentral, checkBoxDriver, checkBoxDelivery, checkBoxAdmin;

    private ArrayList<Central> centralList = new ArrayList<>();
    private ArrayList<Driver> driverList = new ArrayList<>();
    private ArrayList<DeliveryPoint> deliveryPointList = new ArrayList<>();

    public void initialize() throws SQLException {
        root.setOpacity(0);

        //Populate combo boxes.
        centralList = Central.getCentrals();
        comboCentral.getItems().add("0");
        for (Central central : centralList) {
            comboCentral.getItems().add(central.getName());
        }

        driverList = Driver.getDrivers();
        comboDriver.getItems().add("0");
        for (Driver driver : driverList) {
            comboDriver.getItems().add(driver.getName());
        }

        deliveryPointList = DeliveryPoint.getDeliveryPoints();
        comboDelivery.getItems().add("0");
        for (DeliveryPoint deliveryPoint : deliveryPointList) {
            comboDelivery.getItems().add(deliveryPoint.getName());
        }

        new ZoomIn(root).play();

    }

    /**
     * Navigate to statistics page
     */
    public void handleStatistics() {
        Animations.fadeOut(SceneNavigator.ADMIN_PANEL_STATISTICS, root);
    }

    /**
     * Navigate to edit user page
     */
    public void handleEditUser() {
        Animations.fadeOut(SceneNavigator.ADMIN_EDIT_USER, root);
    }

    /**
     * Navigate to new department page
     */
    public void handleNewDepartment() {
        Animations.fadeOut(SceneNavigator.ADMIN_NEW_DEPARTMENT, root);
    }

    /**
     * Create a new user
     * @throws NoSuchAlgorithmException
     * @throws SQLException
     */
    public void handleCreateUser() throws NoSuchAlgorithmException, SQLException {

        String username = txfUsername.getText();
        String password = User.hashPassword(txfPassword.getText());
        long centralId = 0;
        long deliveryPointId = 0;
        long driverId = 0;

        if (comboCentral.getValue().equals("0")) {
            centralId = 0;
        } else {
            for (Central central : centralList) {
                if (central.getName().equals(comboCentral.getValue())) {
                    centralId = central.getId();
                }
            }
        }

        if (comboDelivery.getValue().equals("0")) {
            deliveryPointId = 0;
        } else {
            for (DeliveryPoint deliveryPoint : deliveryPointList) {
                if (deliveryPoint.getName().equals(comboDelivery.getValue())) {
                    deliveryPointId = deliveryPoint.getId();
                }
            }
        }

        if (comboDriver.getValue().equals("0")) {
            driverId = 0;
        } else {
            for (Driver driver : driverList) {
                if (driver.getName().equals(comboDriver.getValue())) {
                    driverId = driver.getDriverId();
                }
            }
        }

        boolean permCentral = checkBoxCentral.isSelected();
        boolean permDriver = checkBoxDriver.isSelected();
        boolean permDelivery = checkBoxDelivery.isSelected();
        boolean permAdmin = checkBoxAdmin.isSelected();

        try {
            if(username == "" || password == "") {
                JFXDialogLayout content = new JFXDialogLayout();
                content.setStyle("-fx-background-color: #272727;");
                Text headline = new Text("Advarsel");
                headline.setStyle("-fx-fill: #FF8C00;");
                content.setHeading(headline);

                Text bodyText = new Text("Udfyld venligst både brugernavn og adgangskode.");
                bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
                content.setBody(bodyText);

                JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
                JFXButton closeButton = new JFXButton("Luk");
                closeButton.setStyle("-fx-fill: #000000");
                closeButton.setStyle("-fx-background-color: #FF8C00");

                closeButton.setOnAction(event -> {
                    dialog.close();
                });

                content.setActions(closeButton);
                dialog.show();

            } else {
                User.createUser(username, password, deliveryPointId, centralId, driverId, permDelivery, permCentral, permDriver, permAdmin);

                JFXDialogLayout content = new JFXDialogLayout();
                content.setStyle("-fx-background-color: #272727;");
                Text headline = new Text("Bruger oprettet");
                headline.setStyle("-fx-fill: #FF8C00;");
                content.setHeading(headline);

                Text bodyText = new Text("Brugeren er blevet oprettet");
                bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
                content.setBody(bodyText);

                JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
                JFXButton closeButton = new JFXButton("Luk");
                closeButton.setStyle("-fx-fill: #000000");
                closeButton.setStyle("-fx-background-color: #FF8C00");

                closeButton.setOnAction(event -> {
                    comboCentral.valueProperty().set(null);
                    comboDriver.valueProperty().set(null);
                    comboDelivery.valueProperty().set(null);

                    checkBoxCentral.setSelected(false);
                    checkBoxDriver.setSelected(false);
                    checkBoxDelivery.setSelected(false);
                    checkBoxAdmin.setSelected(false);

                    txfUsername.clear();
                    txfPassword.clear();
                    dialog.close();
                });

                content.setActions(closeButton);
                dialog.show();
            }

        } catch (Exception e) {
            JFXDialogLayout content = new JFXDialogLayout();
            content.setStyle("-fx-background-color: #272727;");
            Text headline = new Text("Advarsel");
            headline.setStyle("-fx-fill: #FF8C00;");
            content.setHeading(headline);

            Text bodyText = new Text("Brugernavnet eksisterer allerede");
            bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
            content.setBody(bodyText);

            JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
            JFXButton closeButton = new JFXButton("Luk");
            closeButton.setStyle("-fx-fill: #000000");
            closeButton.setStyle("-fx-background-color: #FF8C00");

            closeButton.setOnAction(event -> {
                dialog.close();
            });

            content.setActions(closeButton);
            dialog.show();
        }
    }

    /**
     * Navigate to mainmenu
     */
    public void handleBack() {
        Animations.fadeOut(SceneNavigator.MainMenu, root);
    }

}
