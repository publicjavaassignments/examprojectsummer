package DryCleaning.Application.Admin;

import DryCleaning.Functionality.Animations;
import DryCleaning.Functionality.Central;
import DryCleaning.Functionality.DeliveryPoint;
import DryCleaning.Functionality.Driver;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.ZoomIn;
import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.sql.SQLException;

public class AdminPanelCreateDepartmentController {

    @FXML
    private StackPane root;

    @FXML
    private JFXTextField txfName, txfAddress, txfEmail, txfPhone;
    @FXML
    private JFXComboBox comboType;


    public void initialize() {
        root.setOpacity(0);

        //Populate combobox
        comboType.getItems().add("Central");
        comboType.getItems().add("Leveringspunkt");
        comboType.getItems().add("Chauffør");

        //Disable address text field if type is driver.
        comboType.setOnAction(event -> {
            String type = comboType.getValue().toString();

            if (type.equals("Chauffør")) {
                txfAddress.setEditable(false);
                txfAddress.setOpacity(0.35);
            } else {
                txfAddress.setEditable(true);
                txfAddress.setOpacity(1);
            }
        });
        new ZoomIn(root).play();
    }

    /**
     * Create a new department.
     * @throws SQLException
     */
    public void handleCreateDepartment() throws SQLException {
        String name = txfName.getText();
        String address = txfAddress.getText();
        String email = txfEmail.getText();
        String phone = txfPhone.getText();
        switch (comboType.getValue().toString()) {
            case "Central":
                Central.createDepartment(address, name, email, phone, "tblCentral");
                showDialog("Central");
                break;
            case "Leveringspunkt":
                DeliveryPoint.createDepartment(address, name, email, phone, "tblDeliveryPoint");
                showDialog("Leveringspunkt");
                break;
            case "Chauffør":
                Driver.createDriver(name, phone, email);
                showDialog("Chauffør");
                break;
            default:
                break;
        }
    }

    /**
     * Creates an information dialog.
     * @param departmentType
     */
    private void showDialog(String departmentType) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setStyle("-fx-background-color: #272727;");
        Text headline = new Text("Afdeling oprettet");
        headline.setStyle("-fx-fill: #FF8C00;");
        content.setHeading(headline);

        Text bodyText = new Text(departmentType + " er blevet oprettet");
        bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
        content.setBody(bodyText);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        JFXButton closeButton = new JFXButton("Luk");
        closeButton.setStyle("-fx-fill: #000000");
        closeButton.setStyle("-fx-background-color: #FF8C00");

        closeButton.setOnAction(event -> {
            txfAddress.clear();
            txfName.clear();
            txfEmail.clear();
            txfPhone.clear();

            comboType.setValue(null);

            dialog.close();
        });

        content.setActions(closeButton);
        dialog.show();
    }


    /**
     * Navigate to statistics page.
     */
    public void handleStatistics() {
        Animations.fadeOut(SceneNavigator.ADMIN_PANEL_STATISTICS, root);
    }

    /**
     * Navigate to edit user page.
     */
    public void handleEditUser() {
        Animations.fadeOut(SceneNavigator.ADMIN_EDIT_USER, root);
    }

    /**
     * Navigate to new user page
     */
    public void handleNewUser() {
        Animations.fadeOut(SceneNavigator.ADMIN_CREATE_USER, root);
    }

    /**
     * Navigate to main menu
     */
    public void handleBack() {
        Animations.fadeOut(SceneNavigator.MainMenu, root);
    }

}
