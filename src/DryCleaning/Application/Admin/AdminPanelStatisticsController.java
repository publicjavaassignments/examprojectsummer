package DryCleaning.Application.Admin;

import DryCleaning.Functionality.Animations;
import DryCleaning.Functionality.Statistics;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.ZoomIn;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.layout.StackPane;

import java.sql.SQLException;


public class AdminPanelStatisticsController {

    @FXML
    private StackPane root;
    @FXML
    private LineChart<String, Integer> lineChart;

    public void initialize() throws SQLException {
        root.setOpacity(0);
        handleStatistics();
        new ZoomIn(root).play();
    }

    /**
     * Create linechart
     * @throws SQLException
     */
    public void handleStatistics() throws SQLException {
        lineChart.getData().clear();
        lineChart.getData().add(Statistics.getStatistics());
    }

    /**
     * Navigate to create user page.
     */
    public void handleUserControl() {
        Animations.fadeOut(SceneNavigator.ADMIN_CREATE_USER, root);
    }

    /**
     * Navigate to main menu
     */
    public void handleBack() {
        Animations.fadeOut(SceneNavigator.MainMenu, root);
    }
}
