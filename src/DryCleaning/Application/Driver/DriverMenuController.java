package DryCleaning.Application.Driver;

import DryCleaning.Functionality.*;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.ZoomIn;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXListView;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

public class DriverMenuController {

    @FXML
    private VBox qrRect;
    @FXML
    private JFXListView orderListView;
    @FXML
    private StackPane root;
    @FXML
    private BorderPane BorderPaneFade;

    private long orderId = 0;
    private ArrayList<Long> orderIdList = new ArrayList<>();
    private ArrayList<Long> previousOrderIdList = new ArrayList<>();
    private TransportContent content = new TransportContent();

    public void initialize() throws SQLException {
        BorderPaneFade.setOpacity(0);
        previousOrderIdList = content.populateOrderList(DriverMenuPrelimController.transportId);

        //Populate orders listview.
        for (long orderId : previousOrderIdList) {
            orderListView.getItems().add(new Label("" + orderId));
        }

        handleDragDrop();
        handleOrderInfo();
        new ZoomIn(BorderPaneFade).play();
    }

    /**
     * Create dragboard and sets the status of the order when scanned.
     */
    public void handleDragDrop() {
        try {
            qrRect.setOnDragOver((dragEvent) -> {
                Dragboard db = dragEvent.getDragboard();
                if (db.hasFiles()) {
                    dragEvent.acceptTransferModes(TransferMode.COPY);
                } else {
                    dragEvent.consume();
                }
            });

            qrRect.setOnDragDropped((dragEvent) -> {
                Dragboard db = dragEvent.getDragboard();
                if (db.hasFiles()) {
                    for (File file : db.getFiles()) {
                        try {
                            String qrText = QRCode.read(file);
                            orderId = Long.parseLong(qrText);
                            //Create dialog if the order is already scanned, else increment the order status.
                            if (orderIdList.contains(orderId) || previousOrderIdList.contains(orderId)) {
                                JFXDialogLayout content = new JFXDialogLayout();
                                content.setStyle("-fx-background-color: #272727;");

                                Text headingText = new Text("Advarsel");
                                headingText.setStyle("-fx-fill: #FF8C00;");

                                content.setHeading(headingText);
                                Text bodeText = new Text("Denne ordre er allerede registreret");
                                bodeText.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto;");

                                content.setBody(bodeText);

                                JFXButton closeButton = new JFXButton("Luk");
                                closeButton.setStyle("-fx-background-color: #FF8C00; -fx-fill: #FF8C00");

                                JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
                                closeButton.setOnAction(event -> dialog.close());
                                content.setActions(closeButton);
                                dialog.show();

                            } else {
                                orderListView.getItems().add(new Label(qrText));

                                //Set order status.
                                Order.setStatus(orderId);
                                orderIdList.add(orderId);

                                content = new TransportContent();
                                try {
                                    content.createContent(DriverMenuPrelimController.transportId, orderIdList);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a dialog with info about the selected order.
     */
    public void handleOrderInfo() {
        orderListView.setOnMouseClicked(event -> {
            String orderid = orderListView.getSelectionModel().getSelectedItem().toString();
            orderid = orderid.substring(orderid.indexOf('\'') + 1, orderid.length() - 1);
            long orderIdNum = Long.parseLong(orderid);

            Order order = new Order();
            try {
                order.getDetails(orderIdNum);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            DeliveryPoint deliveryPoint = new DeliveryPoint();
            try {
                deliveryPoint.getDetails(order.getDeliveryPointId());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Customer customer = new Customer();
            try {
                customer.getDetails(order.getCustomerId());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            OrderContent orderContent = new OrderContent();
            try {
                orderContent.getDetails(order.getOrderId());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            JFXDialogLayout content = new JFXDialogLayout();
            Text headline = new Text("Ordre Information");
            headline.setStyle("-fx-fill: #FF8C00;");
            content.setHeading(headline);

            String orderContentString = "";

            for (long categoryId : orderContent.getCategoryIdList()) {
                Category category = new Category();
                try {
                    orderContentString = orderContentString + category.getName(categoryId) + ", ";
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            Text orderContentText = new Text("Indhold: " + orderContentString);
            orderContentText.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto;");

            HBox hBoxOrderId = new HBox();
            Text orderIdText = new Text("Ordrenummer: ");
            orderIdText.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto; -fx-font-weight: bold");
            Text orderIdContent = new Text("" + order.getOrderId());
            orderIdContent.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto;");
            hBoxOrderId.getChildren().addAll(orderIdText, orderIdContent);

            HBox hBoxDeliveryPoint = new HBox();
            Text deliveryPointText = new Text("Butik: ");
            deliveryPointText.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto; -fx-font-weight: bold");
            Text deliveryPointName = new Text("" + deliveryPoint.getName());
            deliveryPointName.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto;");
            hBoxDeliveryPoint.getChildren().addAll(deliveryPointText, deliveryPointName);

            HBox hBoxCustomerName = new HBox();
            Text customerNameText = new Text("Kundenavn: ");
            customerNameText.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto; -fx-font-weight: bold");
            Text customerName = new Text("" + customer.getName());
            customerName.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto;");
            hBoxCustomerName.getChildren().addAll(customerNameText, customerName);

            HBox hBoxCategories = new HBox();
            Text categoriesText = new Text("Ordrenummer: ");
            categoriesText.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto; -fx-font-weight: bold");
            Text categoriesContent = new Text("" + orderContentString);
            categoriesContent.setStyle("-fx-fill: #FF8C00; -fx-font: 16 roboto;");
            hBoxCategories.getChildren().addAll(categoriesText, categoriesContent);

            VBox vBox = new VBox();
            vBox.getChildren().addAll(hBoxOrderId, hBoxDeliveryPoint, hBoxCustomerName, hBoxCategories);

            content.setBody(vBox);
            content.setStyle("-fx-background-color: #272727;");

            JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
            JFXButton closeButton = new JFXButton("Luk");
            closeButton.setStyle("-fx-background-color: #FF8C00; -fx-fill: #FF8C00");

            closeButton.setOnAction(e -> dialog.close());
            content.setActions(closeButton);
            dialog.show();
        });
    }

    /**
     * Navigate back to the main menu.
     */
    public void backButtonPrelim() {
        Animations.fadeOut(SceneNavigator.MainMenu, root);
    }

    /**
     * Displays a dialog where you can choose to finish the route.
     */
    public void handleFinishRoute() {

        JFXDialogLayout content = new JFXDialogLayout();
        content.setStyle("-fx-background-color: #272727;");
        Text headline = new Text("Afslut Rute");
        headline.setStyle("-fx-fill: #FF8C00;");
        content.setHeading(headline);

        Text bodyText = new Text("Er du sikker på, at du vil afslutte denne rute?");
        bodyText.setStyle("-fx-fill: #FF8C00; -fx-font-family: Roboto");
        content.setBody(bodyText);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        JFXButton yesButton = new JFXButton("Ja");
        yesButton.setStyle("-fx-fill: #000000");
        yesButton.setStyle("-fx-background-color: #0e5a1c");

        JFXButton noButton = new JFXButton("Nej");
        noButton.setStyle("-fx-fill: #000000");
        noButton.setStyle("-fx-background-color: #c03a2a");

        yesButton.setOnAction(event -> {
            try {
                Transport.setDone(DriverMenuPrelimController.transportId);
                for (long orderId : orderIdList) {
                    Order.setStatus(orderId);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            //Send pickup email to the customer
            if (DriverMenuPrelimController.buttonClicked.equals("fromCentral")) {
                for (long orderId: orderIdList) {

                    Order order = new Order();
                    try {
                        order.getDetails(orderId);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    long customerId = order.getCustomerId();
                    String readableId = order.getReadableId();

                    DeliveryPoint deliveryPoint = new DeliveryPoint();
                    try {
                        deliveryPoint.getDetails(order.getDeliveryPointId());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    String deliveryPointName = deliveryPoint.getName();


                    Customer customer = new Customer();
                    try {
                        customer.getDetails(customerId);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    String name = customer.getName();
                    String email = customer.getEmail();

                    Thread thread = new Thread(() -> {
                        Mailer.send(email, "AFHENTNING - Dry Cleaning Service", "Kære " + name +
                                "\n\n Dit rensede tøj er nu klar til afhentning hos " + deliveryPointName +
                                "\n Kundenummer: " + customerId +
                                "\n Ordrenummer: " + orderId +
                                "\n\n FREMVIS DETTE ID VED AFHENTNING: " + readableId, "src/QRCodes/" + orderId + ".png");
                    });
                    thread.start();
                }
            }

            Animations.fadeOut(SceneNavigator.MainMenu, root);
            dialog.close();

        });
        noButton.setOnAction(e -> dialog.close());
        content.setActions(yesButton, noButton);
        dialog.show();
    }
}