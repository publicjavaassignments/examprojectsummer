package DryCleaning.Application.Driver;

import DryCleaning.Application.Main.LoginPromptController;
import DryCleaning.Database.HandleID;
import DryCleaning.Functionality.Animations;
import DryCleaning.Functionality.Transport;
import DryCleaning.SceneNavigator;
import Libs.animatefx.animation.FadeInUp;
import Libs.animatefx.animation.FadeOut;
import com.jfoenix.controls.JFXButton;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.sql.SQLException;

import static javafx.animation.Interpolator.EASE_BOTH;

public class DriverMenuPrelimController {

    public static String buttonClicked;
    public static long transportId;

    @FXML
    private BorderPane borderPane;
    @FXML
    private JFXButton btnToCentral, btnFromCentral, backButtonPrelim;
    @FXML
    private Text orderText, pickupText;

    private boolean hasSelected = false;
    private boolean isDone = false;

    public void initialize() {
        transportId = HandleID.getUniqueId();

        btnToCentral.setOpacity(0);
        btnFromCentral.setOpacity(0);
        orderText.setOpacity(0);
        pickupText.setOpacity(0);
        backButtonPrelim.setOpacity(0);

        btnToCentral.setOnMouseExited(t -> handleHoverAnimationDriver(btnToCentral, 1));
        btnToCentral.setOnMouseEntered(t -> handleHoverAnimationDriver(btnToCentral, 0));
        btnToCentral.setOnMousePressed(t -> {
            buttonClicked = "toCentral";
            handleClickAnimationDriver(btnToCentral, 0);
        });

        btnFromCentral.setOnMouseExited(t -> handleHoverAnimationDriver(btnFromCentral, 1));
        btnFromCentral.setOnMouseEntered(t -> handleHoverAnimationDriver(btnFromCentral, 0));
        btnFromCentral.setOnMousePressed(t -> {
            buttonClicked = "fromCentral";
            handleClickAnimationDriver(btnFromCentral, 1);
        });

        Timeline fadeGUI = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(backButtonPrelim.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(pickupText.opacityProperty(),
                        1,
                        EASE_BOTH),
                new KeyValue(orderText.opacityProperty(),
                        1,
                        EASE_BOTH)));

        Thread animationThread = new Thread(() -> {
            try {
                Thread.sleep(300);
                assignAnimationDriver(btnToCentral);
                Thread.sleep(200);
                assignAnimationDriver(btnFromCentral);
                Thread.sleep(500);
                fadeGUI.play();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        animationThread.start();
    }

    /**
     * Navigate back to the main menu.
     */
    public void backButtonDriverprelim() {
        Animations.fadeOut(SceneNavigator.MainMenu, borderPane);
    }

    /**
     * Navigate to either pick up or deliver  order page
     * @param button
     * @param clickedButton
     */
    private void handleClickAnimationDriver(JFXButton button, int clickedButton) {
        hasSelected = true;

        Timeline buttonClick = new Timeline(new KeyFrame(Duration.millis(250),
                new KeyValue(button.scaleXProperty(),
                        1.75,
                        EASE_BOTH),
                new KeyValue(button.scaleYProperty(),
                        1.75,
                        EASE_BOTH)));

        switch (clickedButton) {
            case 0: // btnToCentral
                buttonClick.play();
                new FadeOut(btnFromCentral).play();
                new FadeOut(btnToCentral).play();
                new FadeOut(orderText).play();
                new FadeOut(pickupText).play();
                new FadeOut(backButtonPrelim).play();
                buttonClick.setOnFinished(e -> {
                    try {
                        Transport.createTransport(transportId, LoginPromptController.user.getDriverId(), 2, isDone);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                    SceneNavigator.loadScene(SceneNavigator.DRIVER_MENU);
                });
                break;
            case 1: // btnFromCentral
                buttonClick.play();
                new FadeOut(btnFromCentral).play();
                new FadeOut(btnToCentral).play();
                new FadeOut(orderText).play();
                new FadeOut(pickupText).play();
                new FadeOut(backButtonPrelim).play();

                buttonClick.setOnFinished(e -> {
                    try {
                        Transport.createTransport(transportId, LoginPromptController.user.getDriverId(), 6, isDone);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                    SceneNavigator.loadScene(SceneNavigator.DRIVER_MENU);
                });
                break;
            default:
                break;
        }
    }

    /**
     * Create fadeinup animation
     * @param button
     */
    private void assignAnimationDriver(JFXButton button) {
        new FadeInUp(button).play();
    }

    /**
     * Animation when hovered over
     * @param button
     * @param buttonStatus
     */
    private void handleHoverAnimationDriver(JFXButton button, int buttonStatus) {
        if (hasSelected == true) {
            // Nothing!
        } else {
            Timeline buttonHover = new Timeline(new KeyFrame(Duration.millis(75),
                    new KeyValue(button.scaleXProperty(),
                            1.1,
                            EASE_BOTH),
                    new KeyValue(button.scaleYProperty(),
                            1.1,
                            EASE_BOTH)));

            Timeline buttonUnhover = new Timeline(new KeyFrame(Duration.millis(75),
                    new KeyValue(button.scaleXProperty(),
                            1,
                            EASE_BOTH),
                    new KeyValue(button.scaleYProperty(),
                            1,
                            EASE_BOTH)));

            if (buttonStatus == 0) {
                buttonHover.play();
            } else if (buttonStatus == 1) {
                buttonUnhover.play();
            }
        }
    }
}