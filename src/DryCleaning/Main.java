package DryCleaning;

import DryCleaning.Application.Main.MainController;

import java.io.IOException;

import DryCleaning.Database.SQLServer;
import DryCleaning.Functionality.Category;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        //Populate categories
        SQLServer.connect();
        Category.createCategory("Habit");
        Category.createCategory("Frakke");
        Category.createCategory("Kjole");
        Category.createCategory("Dragt");
        Category.createCategory("Cotton Coat");
        Category.createCategory("Vindjakke");
        Category.createCategory("Bluse");
        Category.createCategory("Nederdel");
        Category.createCategory("Jakke");
        Category.createCategory("Benklæder");
        Category.createCategory("Trøje");
        Category.createCategory("Tæppe");
        Category.createCategory("Gardiner");
        SQLServer.close();

        stage.setTitle("Drycleaning");
        stage.setScene(createScene(loadMainPane()));

        stage.show();
    }

    /**
     *
     * @return
     * @throws IOException
     */
    private Pane loadMainPane() throws IOException {
        FXMLLoader loader = new FXMLLoader();

        Pane mainPane = loader.load(ClassLoader.getSystemResourceAsStream(SceneNavigator.MAIN));

        MainController mainController = loader.getController();

        SceneNavigator.setMainController(mainController);
        SceneNavigator.loadScene(SceneNavigator.MainMenu);

        return mainPane;
    }

    /**
     *
     * @param mainPane
     * @return
     */
    private Scene createScene(Pane mainPane) {
        Scene scene = new Scene(mainPane);
        scene.getStylesheets().setAll(ClassLoader.getSystemResource("DryCleaning/Presentation/style/Theme.css").toExternalForm());

        return scene;
    }
}