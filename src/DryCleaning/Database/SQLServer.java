package DryCleaning.Database;

import java.sql.*;

public class SQLServer {
    public static Connection connect;
    public static Statement statement;
    public static PreparedStatement preparedStatement;

    /**
     * Connect to database
     */
    public static void connect() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connect = DriverManager.getConnection("jdbc:sqlserver://192.168.39.153:1433;databaseName=LaundryDatabase", "sa", "Password123");

            System.out.println("connected");
            DatabaseMetaData metaData = connect.getMetaData();
            System.out.println(metaData.getDriverVersion());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Close database connection
     */
    public static void close() {
        try {
            connect.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
