package DryCleaning.Database;

import java.security.SecureRandom;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.lang.Math.abs;
import static java.lang.System.currentTimeMillis;

public class HandleID {
    private static final Object lock = new Object();
    private static int counter = 0;
    private static long previousUnixTime = 0;
    private static int randomTail = 0;

    /*
    Hashmaps to ensure that two IDs aren't the same within the bounds of the possible generation sizes.
     */
    // Human readable ID Strings
    private static final Map <String, String> previousHRIDs = new LinkedHashMap <String, String> () {
        protected boolean removeEldestEntry(Map.Entry <String, String> eldest) {
            return size() > 6471002; // Maximum amount of unique human readable IDs that can possibly be generated
        }
    };
    // Unique ID Longs
    private static final Map <Long, Long> previousIDs = new LinkedHashMap <Long, Long> () {
        protected boolean removeEldestEntry(Map.Entry <Long, Long> eldest) {
            return size() > 999999; // Maximum amount of unique IDs that can be generated within a millisecond
        }
    };

    /*
    The ID format is as follows: xxxxxxxxxxxxx-yyyy-zz. The X's represent time in milliseconds, making up 13 digits
    of time, which will be solid from the unix epoch, 1'st of January 1970 up until 20'th November, 2286.
    Relative to 2020, that makes the date segment of the ID useable for another 267 years.
    Following is a counter which goes from 0000 to 9999, and a tail of 2 digits which are added through a secure random.
    The Y and Z values are added, as to make it possible to generate more than a single unique ID each millisecond,
    without having collisions. Furthermore, this format is guaranteed to be monotonic in nature,
    meaning it is always increasing.
    */
    /**
     * Generate a unique ID.
     * @return A unique ID as a long.
     */
    public static long getUniqueId() {
        synchronized(lock) {
            long id = generateUniqueID();
            while (previousIDs.containsKey(id)) {
                id = generateUniqueID();
            }
            previousIDs.put(id, null);
            return id;
        }
    }

    /**
     * Generate a unique human readable ID.
     * @return A unique human readable ID as a string.
     */
    public static String getUniqueHumanReadableID() {
        synchronized(lock) {
            String id = generateUniqueHumanReadableID();
            while (previousHRIDs.containsKey(id)) {
                id = generateUniqueHumanReadableID();
            }
            previousHRIDs.put(id, null);
            return id;
        }
    }

    // While the ID itself gives us the possibility of retrieving the generation date, actually using it for both ID and
    // date within the database violates the atomicity principle of database design. (ACID properties)
    // https://dl.acm.org/doi/pdf/10.1145/289.291
    /**
     * Find out when the ID was generated.
     * @param dateID a long value which was generated using getUniqueID().
     * @return A date object containing the time the ID was generated.
     */
    public static Date getDate(long dateID) {
        return new Date(dateID / 1000000);
    }

    /*
    Turns out generating a 19 digit long unique ID is faster than an 18 digit unique ID, as waiting for the milliseconds
    to tick over in a loop can take time. Using this format is faster.
     */
    private static long generateUniqueID() {
        {
            counter++;
            if (counter >= 10000) {
                counter = 0;
            }

            long currentTimeMilliseconds = currentTimeMillis();

            if (currentTimeMilliseconds > previousUnixTime) {
                counter = 0;
                previousUnixTime = currentTimeMilliseconds;
            }

            SecureRandom random = new SecureRandom();
            randomTail = abs(random.nextInt()) % 100;

            return currentTimeMilliseconds * 1000000 + counter * 100 + randomTail;
        }
    }

    /*
    Returns a 5 character long string, containing characters from the predefined charset.
    Calculating the combinations, C(n,r)= 6471002 unique combinations.

    This ID is only to be used to quickly identify packages within the warehouse whenever the customers comes to
    pick up their clothes again. Because of this, collisions are not an issue (even though they're unlikely to occur)
    since the UniqueID (Which is a function of time, which always moves forward) will be checked against the customers
    receipt upon pickup.
     */
    private static String generateUniqueHumanReadableID() {
        {
            String charSet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int IDLength = 5;

            SecureRandom secureRandom = new SecureRandom();

            // Apache Commons 'RandomStringUtils' one-liner replacement
            String id = secureRandom.ints(IDLength, 0, charSet.length()).mapToObj(charSet::charAt).collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();

            return id;
        }
    }
}