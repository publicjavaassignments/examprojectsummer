CREATE DATABASE [LaundryDatabase]
USE [LaundryDatabase]

CREATE TABLE [dbo].[tblCentral](
    [CentralID] [bigint] NOT NULL,
    [Address] [varchar](250) NULL,
    [Name] [varchar](250) NULL,
    [Email] [varchar](250) NULL,
    [PhoneNumber] [varchar](20) NULL,
CONSTRAINT [PK_tblCentral] PRIMARY KEY CLUSTERED ([CentralID] ASC)
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
   ALLOW_PAGE_LOCKS = ON)
   ON [PRIMARY])
ON [PRIMARY]

CREATE TABLE [dbo].[tblCustomer](
    [CustomerID] [bigint] NOT NULL,
    [PhoneNumber] [varchar](20) NULL,
    [Email] [varchar](250) NULL,
    [Name] [varchar](250) NULL,
CONSTRAINT [PK_tblCustomer] PRIMARY KEY CLUSTERED ([CustomerID] ASC)
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
   ALLOW_PAGE_LOCKS = ON)
   ON [PRIMARY])
ON [PRIMARY]

CREATE TABLE [dbo].[tblDeliveryPoint](
    [DeliveryPointID] [bigint] NOT NULL,
    [Address] [varchar](250) NULL,
    [Name] [varchar](250) NULL,
    [Email] [varchar](250) NULL,
    [PhoneNumber] [varchar](20) NULL,
CONSTRAINT [PK_tblDeliveryPoint] PRIMARY KEY CLUSTERED ([DeliveryPointID] ASC)
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
   ALLOW_PAGE_LOCKS = ON)
   ON [PRIMARY])
ON [PRIMARY]

CREATE TABLE [dbo].[tblDriver](
    [DriverID] [bigint] NOT NULL,
    [Name] [varchar](100) NULL,
    [PhoneNumber] [varchar](20) NULL,
    [Email] [varchar](100) NULL,
CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED ([DriverID] ASC)
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
   ALLOW_PAGE_LOCKS = ON)
   ON [PRIMARY])
ON [PRIMARY]

CREATE TABLE [dbo].[tblLaundryCategory](
    [CategoryID] [bigint] NOT NULL,
    [Name] [varchar](100) NULL,
CONSTRAINT [PK_tblLaundryCategory] PRIMARY KEY CLUSTERED ([CategoryID] ASC)
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
   ALLOW_PAGE_LOCKS = ON)
   ON [PRIMARY])
ON [PRIMARY]

CREATE TABLE [dbo].[tblOrder](
    [OrderID] [bigint] NOT NULL,
    [Date] [datetime] NULL,
    [CentralID] [bigint] NULL,
    [Status] [int] NULL,
    [DeliveryPointID] [bigint] NULL,
    [CustomerID] [bigint] NULL,
    [ReadableID] [varchar](5) NULL,
CONSTRAINT [PK_tblOrder] PRIMARY KEY CLUSTERED ([OrderID] ASC)
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
   ALLOW_PAGE_LOCKS = ON)
   ON [PRIMARY])
ON [PRIMARY]

CREATE TABLE [dbo].[tblOrderContent](
    [OrderID] [bigint] NULL,
    [CategoryID] [bigint] NULL,
    [Amount] [int] NULL)
ON [PRIMARY]

CREATE TABLE [dbo].[tblTransport](
    [TransportID] [bigint] NOT NULL,
    [DriverID] [bigint] NULL,
    [Date] [datetime] NULL,
    [Status] [int] NULL,
    [isDone] [bit] NULL,
CONSTRAINT [PK_tblTransport] PRIMARY KEY CLUSTERED ([TransportID] ASC)
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
   ALLOW_PAGE_LOCKS = ON)
   ON [PRIMARY])
ON [PRIMARY]

CREATE TABLE [dbo].[tblTransportContent](
    [TransportID] [bigint] NULL,
    [OrderID] [bigint] NULL)
ON [PRIMARY]

CREATE TABLE [dbo].[tblUser](
    [Username] [varchar](100) NULL,
    [Password] [varchar](100) NULL,
    [DeliveryPointID] [bigint] NULL,
    [CentralID] [bigint] NULL,
    [DriverID] [bigint] NULL,
    [PermDeliveryPoint] [bit] NULL,
    [PermCentral] [bit] NULL,
    [PermDriver] [bit] NULL)
   [PermAdmin] [bit] NULL,
PRIMARY KEY CLUSTERED ([Username] ASC)
   WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
   ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])
ON [PRIMARY]

ALTER TABLE
   [dbo].[tblOrder]  WITH CHECK ADD  CONSTRAINT [FK_tblOrder_tblCentral] FOREIGN KEY ([CentralID])
       REFERENCES [dbo].[tblCentral] ([CentralID])

ALTER TABLE
   [dbo].[tblOrder] CHECK CONSTRAINT [FK_tblOrder_tblCentral]

ALTER TABLE
   [dbo].[tblOrder]  WITH CHECK ADD  CONSTRAINT [FK_tblOrder_tblCustomer] FOREIGN KEY ([CustomerID])
       REFERENCES [dbo].[tblCustomer] ([CustomerID])

ALTER TABLE
   [dbo].[tblOrder] CHECK CONSTRAINT [FK_tblOrder_tblCustomer]

ALTER TABLE
   [dbo].[tblOrder]  WITH CHECK ADD  CONSTRAINT [FK_tblOrder_tblDeliveryPoint] FOREIGN KEY ([DeliveryPointID])
       REFERENCES [dbo].[tblDeliveryPoint] ([DeliveryPointID])


ALTER TABLE
   [dbo].[tblOrder] CHECK CONSTRAINT [FK_tblOrder_tblDeliveryPoint]

ALTER TABLE
   [dbo].[tblOrderContent]  WITH CHECK ADD  CONSTRAINT [FK_tblOrderContent_tblLaundryCategory] FOREIGN KEY ([CategoryID])
       REFERENCES [dbo].[tblLaundryCategory] ([CategoryID])

ALTER TABLE
   [dbo].[tblOrderContent] CHECK CONSTRAINT [FK_tblOrderContent_tblLaundryCategory]

ALTER TABLE
   [dbo].[tblOrderContent]  WITH CHECK ADD  CONSTRAINT [FK_tblOrderContent_tblOrder] FOREIGN KEY ([OrderID])
       REFERENCES [dbo].[tblOrder] ([OrderID])

ALTER TABLE
   [dbo].[tblOrderContent] CHECK CONSTRAINT [FK_tblOrderContent_tblOrder]

ALTER TABLE
   [dbo].[tblTransport]  WITH CHECK ADD  CONSTRAINT [FK_tblTransport_tblDriver] FOREIGN KEY ([DriverID])
       REFERENCES [dbo].[tblDriver] ([DriverID])

ALTER TABLE
   [dbo].[tblTransport] CHECK CONSTRAINT [FK_tblTransport_tblDriver]

ALTER TABLE
   [dbo].[tblTransportContent]  WITH CHECK ADD  CONSTRAINT [FK_tblTransportContent_tblOrder] FOREIGN KEY ([OrderID])
       REFERENCES [dbo].[tblOrder] ([OrderID])

ALTER TABLE
   [dbo].[tblTransportContent] CHECK CONSTRAINT [FK_tblTransportContent_tblOrder]

ALTER TABLE
   [dbo].[tblTransportContent]  WITH CHECK ADD  CONSTRAINT [FK_tblTransportContent_tblTransport] FOREIGN KEY ([TransportID])
       REFERENCES [dbo].[tblTransport] ([TransportID])

ALTER TABLE
   [dbo].[tblTransportContent] CHECK CONSTRAINT [FK_tblTransportContent_tblTransport]

ALTER TABLE
   [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_tblCentral] FOREIGN KEY ([CentralID])
       REFERENCES [dbo].[tblCentral] ([CentralID])

ALTER TABLE
   [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_tblCentral]

ALTER TABLE
   [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_tblDeliveryPoint] FOREIGN KEY ([DeliveryPointID])
       REFERENCES [dbo].[tblDeliveryPoint] ([DeliveryPointID])

ALTER TABLE
   [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_tblDeliveryPoint]

ALTER TABLE
   [dbo].[tblUser]  WITH CHECK ADD CONSTRAINT [FK_tblUser_tblDriver] FOREIGN KEY ([DriverID])
       REFERENCES [dbo].[tblDriver] ([DriverID])

ALTER TABLE
   [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_tblDriver]

CREATE PROCEDURE [dbo].[insertCategory](@CategoryID BIGINT, @Name VARCHAR(100))
AS
    BEGIN
        IF NOT EXISTS (SELECT * FROM tblLaundryCategory WHERE CategoryID = @CategoryID OR Name =  @Name)
        INSERT INTO tblLaundryCategory (CategoryID, Name) VALUES (@CategoryID, @Name)
    END

CREATE PROCEDURE [dbo].[SetStatus] @OrderID BIGINT
AS
   DECLARE @Count INT
       SET @Count = (SELECT Status FROM tblOrder WHERE OrderID = @OrderID)

   IF @Count < 9
       SET @Count = @Count + 1
           UPDATE tblOrder SET Status = @Count WHERE OrderID = @OrderID