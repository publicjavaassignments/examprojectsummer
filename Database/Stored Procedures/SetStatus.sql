USE LaundryDatabase

GO
CREATE PROCEDURE SetStatus @OrderID BIGINT
AS
DECLARE @Count INT
SET @Count = (SELECT Status FROM tblOrder WHERE OrderID = @OrderID)

IF @Count < 9
	SET @Count = @Count + 1
	UPDATE tblOrder SET Status = @Count WHERE OrderID = @OrderID
