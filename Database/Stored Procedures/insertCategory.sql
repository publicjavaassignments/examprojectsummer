USE LaundryDatabase
GO
CREATE PROCEDURE insertCategory(@CategoryID BIGINT, @Name VARCHAR(100)) AS BEGIN IF NOT EXISTS
    (SELECT * FROM
            tblLaundryCategory
        WHERE
            CategoryID = @CategoryID OR Name = @Name)
    INSERT INTO
        tblLaundryCategory (CategoryID, Name)
    VALUES
    (@CategoryID, @Name)
END